/*
 * Copyright (c) 2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__OPTIMIZER__H
#define __WALDO__OPTIMIZER__H

#define WALDO_OK_OPTIMIZER_CONVERGE 		0 /**< Converge */
#define WALDO_OK_OPTIMIZER_DIVERGE 		1 /**< Diverge  */


typedef struct waldo_optimizer_limits {
    waldo_real_t epsilon_x;
    waldo_real_t epsilon_fx;
    int max_iter;
    int max_eval;
} waldo_optimizer_limits_t;


typedef struct waldo_optimizer_stats {
    waldo_real_t delta_x;
    waldo_real_t delta_fx;
    int iter;
    int eval;    
} waldo_optimizer_stats_t;


/**
 * Point (x = argument, fx = function value)
 */
typedef struct waldo_optimizer_point {
    waldo_real_t *x;
    waldo_real_t fx;
} waldo_optimizer_point_t;

/**
 * Simplex of dimension n.
 * Contains n+1 points.
 */
typedef struct waldo_optimizer_simplex {
    int n;              /* Simplex dimension */
    waldo_optimizer_point_t *point;
} waldo_optimizer_simplex_t;

/**
 * Cost function.
 
 * @param n	dimensions
 * @param x	coordinates
 * @param args  extra arguments
 * @return cost
 */
typedef waldo_real_t (*waldo_optimizer_cost_function_t)(int n, waldo_real_t *x, const void *args);


/**
 * Perform Nelder-Mead optimization.
 * Multidimensional unconstrained optimization without derivatives.
 *
 * @param n			dimension
 * @param cost_function		cost function
 * @param cost_args		optional arguments of the cost function
 * @param start			starting coordinates 
 * @param estimate [out]	estimated coordinates
 * @param limits		limits for convergence/divergence termination
 * @param stats			(optional)
 *
 * @return WALDO_OK_OPTIMIZER_CONVERGE
 * @return WALDO_OK_OPTIMIZER_DIVERGE
 */
int waldo_optimizer_nelder_mead(int n,
				waldo_optimizer_cost_function_t cost_function,
				const void                     *cost_args,
				const waldo_real_t             *start,
				waldo_real_t                   *estimate,
				const waldo_optimizer_limits_t *limits,
				waldo_optimizer_stats_t        *stats);

#endif
