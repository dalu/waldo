/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__MATRIX__H
#define __WALDO__MATRIX__H


#define WALDO_MATRIX_INV_LUP  1
#define WALDO_MATRIX_INV_RREF 2

#ifndef WALDO_MATRIX_INV 
#define WALDO_MATRIX_INV WALDO_MATRIX_INV_RREF
#endif

#ifndef WALDO_MATRIX_WITH_CAPACITY
#define WALDO_MATRIX_WITH_CAPACITY 1
#endif

#ifndef WALDO_MATRIX_WITH_SANITY_CHECK
#define WALDO_MATRIX_WITH_SANITY_CHECK 1
#endif

#include <stdlib.h>

#include <waldo.h>
#include <waldo/coordinates.h>

// row major (not like blas/lapack)
typedef struct waldo_matrix {
    union {
	int size[2];
	struct {
	    int rows;
	    int columns;
	};
	struct {
	    int m;
	    int n;
	};
    };

    waldo_real_t *data;

#if WALDO_MATRIX_WITH_CAPACITY 
    unsigned int capacity;
#endif
} waldo_matrix_t;


#define WALDO_MATRIX_DEF(name, rows, columns);			\
    waldo_real_t   name ## _data_[(rows) * (columns)];		\
    waldo_matrix_t name = {					\
	.size = { (rows), (columns) },				\
	.data = name ## _data_					\
    }

#define WALDO_MATRIX_SQUARE_DEF(name, size);			\
    WALDO_MATRIX_DEF(name, size, size)

#define WALDO_MATRIX_DEF_FROM(name, m);				\
    WALDO_MATRIX_DEF(name, (m)->rows, (m)->columns)

#define WALDO_MATRIX_DEF_FROM_TRANSPOSE(name, m);		\
    WALDO_MATRIX_DEF(name, (m)->columns, (m)->rows)

#define WALDO_MATRIX_DEF_FROM_MULT(name, a, b);		\
    WALDO_MATRIX_DEF(name, (a)->rows, (b)->columns)


#ifndef WALDO_MATRIX_EPSILON
#define WALDO_MATRIX_EPSILON WALDO_EPSILON
#endif


#define WALDO_MATRIC_ALLOC(capacity)					\
    _WALDO_FLEXARRAY_ALLOCSIZE(struct waldo_matrix, data, capacity)

#define WALDO_MATRIX_ALLOCSIZE(row, columns)				\
    WALDO_MATRIC_ALLOC((rows) * (columns))




static inline
int waldo_matrix_resize(waldo_matrix_t *m,
			unsigned short rows, unsigned short columns) {
    m->rows    = rows;
    m->columns = columns;
#if WALDO_MATRIX_WITH_CAPACITY 
    if (m->capacity < rows * columns)
	return WALDO_ERR_NO_MEMORY;
#endif
    return WALDO_OK;
    return WALDO_ERR_NOT_SUPPORTED;
}


#define WALDO_MATRIX_ELT_INDEX(m, i, j)					\
    ((i) * (m)->columns + (j))

#define WALDO_MATRIX_ELT(m, i, j)					\
    (m)->data[WALDO_MATRIX_ELT_INDEX(m, i, j)]



#define WALDO_MATRIX_IS_DEFINED(m)					\
    (((m) != NULL) && ((m)->rows > 0) && ((m)->columns > 0))

#define WALDO_MATRIX_IS_UNDEFINED(m)					\
    (((m) == NULL) || ((m)->rows <= 0) || ((m)->columns <= 0))

#define WALDO_MATRIX_IS_SAME_ROW_SIZE(a, b)				\
    ((a)->rows    == (b)->rows   )

#define WALDO_MATRIX_IS_SAME_COLUMN_SIZE(a, b)				\
    ((a)->columns == (b)->columns)

#define WALDO_MATRIX_IS_SAME_SIZE(a, b)					\
    (((a)->rows    == (b)->rows   ) &&					\
     ((a)->columns == (b)->columns))

#define WALDO_MATRIX_IS_SIZE(a, m, n)					\
    (((a)->rows == (m)) && ((a)->columns == (n)))

#define WALDO_MATRIX_IS_SQUARED_SIZE(a, s)				\
    (((a)->rows == (s)) && ((a)->columns == (s)))

#define WALDO_MATRIX_IS_TRANSPOSED_SIZE(a, b)				\
    (((a)->rows    == (b)->columns) &&					\
     ((a)->columns == (b)->rows   ))

#define WALDO_MATRIX_IS_SQUARE(m)					\
    ((m)->rows == (m)->columns)


#if WALDO_MATRIX_WITH_SANITY_CHECK

#define WALDO_ASSERT_MATRIX_IS_DEFINED(m)				\
    WALDO_ASSERT(WALDO_MATRIX_IS_DEFINED(m))

#define WALDO_ASSERT_MATRIX_IS_SQUARE(m)				\
    WALDO_ASSERT(WALDO_MATRIX_IS_SQUARE(m))

#define WALDO_ASSERT_MATRIX_IS_SAME_ROW_SIZE(a, b)			\
    WALDO_ASSERT(WALDO_MATRIX_IS_SAME_ROW_SIZE(a, b))

#define WALDO_ASSERT_MATRIX_IS_SAME_COLUMN_SIZE(a, b)			\
    WALDO_ASSERT(WALDO_MATRIX_IS_SAME_COLUMN_SIZE(a, b))

#define WALDO_ASSERT_MATRIX_IS_SAME_SIZE(a, b)				\
    WALDO_ASSERT(WALDO_MATRIX_IS_SAME_SIZE(a, b))

#define WALDO_ASSERT_MATRIX_IS_SIZE(a, m, n)				\
    WALDO_ASSERT(WALDO_MATRIX_IS_SIZE(a, m, n))

#define WALDO_ASSERT_MATRIX_IS_SQUARED_SIZE(a, s)			\
    WALDO_ASSERT(WALDO_MATRIX_IS_SQUARED_SIZE(a, s))

#define WALDO_ASSERT_MATRIX_IS_TRANSPOSED_SIZE(a, b)			\
    WALDO_ASSERT(WALDO_MATRIX_IS_TRANSPOSED_SIZE(a, b))

#else

#define WALDO_ASSERT_MATRIX_IS_DEFINED(m)
#define WALDO_ASSERT_MATRIX_IS_SQUARE(m)
#define WALDO_ASSERT_MATRIX_IS_SAME_ROW_SIZE(a, b)
#define WALDO_ASSERT_MATRIX_IS_SAME_COLUMN_SIZE(a, b)
#define WALDO_ASSERT_MATRIX_IS_SAME_SIZE(a, b)
#define WALDO_ASSERT_MATRIX_IS_SIZE(a, m, n)
#define WALDO_ASSERT_MATRIX_IS_SQUARED_SIZE(a, s)
#define WALDO_ASSERT_MATRIX_IS_TRANSPOSED_SIZE(a, b)

#endif


static inline void
waldo_matrix_copy(waldo_matrix_t *dst, waldo_matrix_t *src)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(dst);
    WALDO_ASSERT_MATRIX_IS_DEFINED(src);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(dst, src);

    for (int i = 0 ; i < src->rows ; i++)
	for (int j = 0 ; j < src->columns ; j++)
	    WALDO_MATRIX_ELT(dst, i, j) = WALDO_MATRIX_ELT(src, i, j);
}

/**
 */

static inline void
waldo_matrix_sum_row(waldo_matrix_t *r, waldo_matrix_t *m) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SAME_COLUMN_SIZE(r, m);

    for (int j = 0 ; j < m->columns ; j++) {
	WALDO_MATRIX_ELT(r, 0, j) = 0.0;
	for (int i = 0 ; i < m->rows ; i ++) 
	    WALDO_MATRIX_ELT(r, 0, j) += WALDO_MATRIX_ELT(m, i, j);
    }
}

static inline void
waldo_matrix_sum_column(waldo_matrix_t *r, waldo_matrix_t *m) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SAME_ROW_SIZE(r, m);

    for (int i = 0 ; i < m->rows ; i ++) {
	WALDO_MATRIX_ELT(r, i, 0) = 0.0;
	for (int j = 0 ; j < m->columns ; j++)
	    WALDO_MATRIX_ELT(r, i, 0) += WALDO_MATRIX_ELT(m, i, j);
    }
}


static inline void
waldo_matrix_swap_row(waldo_matrix_t *m, int i1, int i2)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int j = 0 ; j < m->columns ; j++)
	WALDO_SWAP(WALDO_MATRIX_ELT(m, i1, j), WALDO_MATRIX_ELT(m, i2, j));
}

static inline void
waldo_matrix_swap_column(waldo_matrix_t *m, int j1, int j2)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int i = 0 ; i < m->rows ; i++)
	WALDO_SWAP(WALDO_MATRIX_ELT(m, i, j1), WALDO_MATRIX_ELT(m, i, j2));
}


static inline void
waldo_matrix_set_row(waldo_matrix_t *m, int i, waldo_real_t *data) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int j = 0 ; j < m->columns ; j++)
	WALDO_MATRIX_ELT(m, i, j) = data[j];
}

static inline void
waldo_matrix_set_column(waldo_matrix_t *m, int j, waldo_real_t *data) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int i = 0 ; i < m->rows ; i++)
	WALDO_MATRIX_ELT(m, i, j) = data[i];
}


static inline void
waldo_matrix_get_row(waldo_matrix_t *m, int i, waldo_real_t *data) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int j = 0 ; j < m->columns ; j++)
	data[j] = WALDO_MATRIX_ELT(m, i, j);
}

static inline void
waldo_matrix_get_column(waldo_matrix_t *m, int j, waldo_real_t *data) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int i = 0 ; i < m->rows ; i++)
	data[i] = WALDO_MATRIX_ELT(m, i, j);
}

static inline void
waldo_matrix_get_diag(waldo_matrix_t *m, waldo_real_t *data) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SQUARE(m); 
    
    for (int i = 0 ; i < m->rows ; i++)
	data[i] = WALDO_MATRIX_ELT(m, i, i);
}

static inline void
waldo_matrix_set_diag(waldo_matrix_t *m, waldo_real_t *data) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SQUARE(m); 
    
    for (int i = 0 ; i < m->rows ; i++)
	WALDO_MATRIX_ELT(m, i, i) = data[i];
}


typedef waldo_real_t (waldo_matrix_op_binary_t)(waldo_real_t a, waldo_real_t b);
typedef waldo_real_t (waldo_matrix_op_unary_t)(waldo_real_t a);



					      
static inline void
waldo_matrix_each_sq(waldo_matrix_t *r, waldo_matrix_t *m) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(r, m);

    for (int i = 0 ; i < m->rows ; i++) 
	for (int j = 0 ; j < m->columns ; j++)
	    WALDO_MATRIX_ELT(r, i, j) = WALDO_MATRIX_ELT(m, i, j) *
		                        WALDO_MATRIX_ELT(m, i, j);
}

static inline int
waldo_matrix_each(waldo_matrix_t *r, waldo_matrix_t *a, waldo_matrix_op_unary_t *f) {
    for (int i = 0 ; i < a->rows ; i++) {
	for (int j = 0 ; j < a->columns ; j++) {
	    waldo_real_t v = f(WALDO_MATRIX_ELT(a, i, j));
	    if (isnan(v)) return -EINVAL;
	    WALDO_MATRIX_ELT(r, i, j) = v;
	}
    }
    return 0;
}


static inline void
waldo_matrix_div_row(waldo_matrix_t *m, int i, waldo_real_t s) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int j = 0 ; j < m->columns ; j++)
	WALDO_MATRIX_ELT(m, i, j) /= s;
}

static inline void
waldo_matrix_axpy_row(waldo_matrix_t *r, int i_r,
		      waldo_real_t a,
		      waldo_matrix_t *x, int i_x,
		      waldo_matrix_t *y, int i_y) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(x);
    WALDO_ASSERT_MATRIX_IS_DEFINED(y);
    

    for (int j = 0 ; j < r->columns ; j++)
	WALDO_MATRIX_ELT(r, i_r, j) =
	    a * WALDO_MATRIX_ELT(x, i_x, j) + WALDO_MATRIX_ELT(y, i_y, j);
}


static inline void
waldo_matrix_mk_diag_1(waldo_matrix_t *m) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int i = 0 ; i < m->rows ; i++)
	for (int j = 0 ; j < m->columns ; j++)
	    WALDO_MATRIX_ELT(m, i, j) = (i == j) ? 1.0 : 0.0;
}


static inline void
waldo_matrix_mk_zero(waldo_matrix_t *m) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);

    for (int i = 0 ; i < m->rows ; i++)
	for (int j = 0 ; j < m->columns ; j++)
	    WALDO_MATRIX_ELT(m, i, j) = 0.0;
}






/**
 * Compute matrix A + B
 * @note In-place compatible. (for square matrix only)
 *
 * @param r	Pointer to the result matrix
 * @param a	Pointer to matrix A
 * @param b	Pointer to matrix B
 */
static inline void
waldo_matrix_transpose(waldo_matrix_t *r, waldo_matrix_t *a)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(a);
    WALDO_ASSERT_MATRIX_IS_TRANSPOSED_SIZE(r, a);

    // If in-place square matrix
    if (r == a) {
	for (int i = 0 ; i < a->rows ; i++)
	    for (int j = i + 1 ; j < a->columns ; j++)
		WALDO_SWAP(WALDO_MATRIX_ELT(a, j, i),
			   WALDO_MATRIX_ELT(a, i, j));

    // Otherwise
    } else {
	for (int i = 0 ; i < a->rows ; i++)
	    for (int j = 0 ; j < a->columns ; j++)
		WALDO_MATRIX_ELT(r, j, i) = WALDO_MATRIX_ELT(a, i, j);
    }
}


/**
 * Compute matrix A + B
 * @note In-place compatible.
 *
 * @param r	Pointer to the result matrix
 * @param a	Pointer to matrix A
 * @param b	Pointer to matrix B
 */
static inline void
waldo_matrix_add(waldo_matrix_t *r, waldo_matrix_t *a, waldo_matrix_t *b)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(a);
    WALDO_ASSERT_MATRIX_IS_DEFINED(b);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(a, b);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(r, a);

    for (int i = 0 ; i < a->rows ; i++)
	for (int j = 0 ; j < a->columns ; j++)
	    WALDO_MATRIX_ELT(r, i, j) =
		WALDO_MATRIX_ELT(a, i, j) + WALDO_MATRIX_ELT(b, i, j);
}

/**
 * Compute matrix A - B
 * @note In-place compatible.
 *
 * @param r	Pointer to the result matrix
 * @param a	Pointer to matrix A
 * @param b	Pointer to matrix B
 */
static inline void
waldo_matrix_sub(waldo_matrix_t *r, waldo_matrix_t *a, waldo_matrix_t *b)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(a);
    WALDO_ASSERT_MATRIX_IS_DEFINED(b);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(a, b);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(r, a);

    for (int i = 0 ; i < a->rows ; i++)
	for (int j = 0 ; j < a->columns ; j++)
	    WALDO_MATRIX_ELT(r, i, j) =
		WALDO_MATRIX_ELT(a, i, j) - WALDO_MATRIX_ELT(b, i, j);
}

/**
 * Compute matrix A * B
 *
 * @param r	Pointer to the result matrix
 * @param a	Pointer to matrix A
 * @param b	Pointer to matrix B
 */
static inline void
waldo_matrix_mult(waldo_matrix_t *r, waldo_matrix_t *a, waldo_matrix_t *b)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(a);
    WALDO_ASSERT_MATRIX_IS_DEFINED(b);
    WALDO_ASSERT(r != a);
    WALDO_ASSERT(r != b);
    WALDO_ASSERT(a->columns == b->rows);
    WALDO_ASSERT((r->rows == a->rows) && (r->columns == b->columns));
    
    for (int i = 0 ; i < a->rows ; i++) {
	for (int j = 0 ; j < b->columns ; j++) {
	    waldo_real_t v = 0;
	    for (int k = 0 ; k < a->columns ; k++)
		v += WALDO_MATRIX_ELT(a, i, k) * WALDO_MATRIX_ELT(b, k, j);
	    WALDO_MATRIX_ELT(r, i, j) = v;
	}
    }
}

/**
 * Compute the LU matrix and the associated permutation array.
 * @note In-place compatible.
 *
 * @param r	Pointer to the result matrix
 * @param a	Pointer to a square matrix
 * @param v	Multiplication factor
 */
static inline void
waldo_matrix_scale(waldo_matrix_t *r, waldo_matrix_t *a, waldo_real_t v)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(a);
    
    for (int i = 0 ; i < a->rows ; i++)
	for (int j = 0 ; j < a->columns ; j++)
	    WALDO_MATRIX_ELT(r, i, j) = v * WALDO_MATRIX_ELT(a, i, j);
}

/**
 * Compute the LU matrix and the associated permutation array.
 * @note In-place compatible.
 *
 * @param r	Pointer to the result LU matrix
 * @param a	Pointer to a square matrix
 * @param p	Pointer to a permutation array
 *
 * @return the number of permutations on succes, and < 0 on failure
 */
int waldo_matrix_lup(waldo_matrix_t *r, waldo_matrix_t *a, int *p);

/**
 * Compute the inverted matrix from an already decomposed LUP.
 *
 * @param r	Pointer to the result LU matrix
 * @param lu	Pointer to a square LU matrix
 * @param p	Pointer to a permutation array
 */
void waldo_matrix_inv_lup(waldo_matrix_t *r, waldo_matrix_t *lu, int *p);

/**
 * Compute the matrix determinant from an already decomposed LUP.
 *
 * @param lu	Pointer to a square LU matrix
 * @param perms	Number of permutations
 *
 * @return the matrix determinant
 */
waldo_real_t waldo_matrix_det_lup(waldo_matrix_t *lu, int perms);

/**
 * Reduced Row Echelon Form.
 *
 * If `a` is set to the identity matrix and the matrix is invertible,
 * `a` will hold the inverted matrix, and `rref` the identity matrix.
 * 
 *
 * @note In-place compatible.
 *
 * @param rref	Pointer to the result reduced row echelon form matrix
 * @param m	Pointer to a matrix
 * @param a     Pointer to another matrix (optional)
 *
 * @return determinant (set to 0.0 if matrix is not square)
 */
waldo_real_t
waldo_matrix_rref(waldo_matrix_t *rref, waldo_matrix_t *m, waldo_matrix_t *a);

/**
 * Invert a (square) matrix.
 * @note In-place compatible.
 * @note stack-allocated matrix.
 *
 * @param r	Pointer to the result inverted-matrix
 * @param m	Pointer to a square matrix
 *
 * @return <0 on failure.
 */
#if WALDO_MATRIX_INV == WALDO_MATRIX_INV_LUP
static inline int
waldo_matrix_inv(waldo_matrix_t *r, waldo_matrix_t *m)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SQUARE(m);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(r, m);
    
    // Matrix size
    const int n = m->rows;

    // Stack allocation for LU P
    int            p[n];
    waldo_real_t   lu_data[n*n];
    waldo_matrix_t lu = { .size = { n, n },
			  .data = lu_data };

    // LUP decomposition
    //  (fails if not invertible)
    int rc = waldo_matrix_lup(&lu, m, p);
    if (rc < 0) return rc;

    // Invert matrix
    waldo_matrix_inv_lup(r, &lu, p);

    // Done
    return WALDO_OK;
}
#elif WALDO_MATRIX_INV == WALDO_MATRIX_INV_RREF
static inline int
waldo_matrix_inv(waldo_matrix_t *r, waldo_matrix_t *m)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SQUARE(m);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(r, m);
    
    // Matrix size
    const int n = m->rows;

    // Stack allocation for RREF
    waldo_real_t   rref_data[n*n];
    waldo_matrix_t rref = { .size = { n, n },
			    .data = rref_data };

    // Initialise r as a diagonal matrix
    waldo_matrix_mk_diag_1(r);

    // Perform row echelon reduction with identity as adjacent matrice
    waldo_real_t det = waldo_matrix_rref(&rref, m, r);
    
    // Done
    return det == 0.0 ? WALDO_ERR_MATH : WALDO_OK;
}
#else
#error Matrix inversion method not defined
#endif




void waldo_matrix_dump_fmt(waldo_matrix_t *m, char *prefix, char *fmt);

static inline void waldo_matrix_dump(waldo_matrix_t *m, char *prefix) {
    waldo_matrix_dump_fmt(m, prefix, "%8.4e");
}

#endif
