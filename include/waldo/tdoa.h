/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__TDOA__H
#define __WALDO__TDOA__H


/**
 * Solving TDOA using Nelder-Mead optimization algorithm.
 */
int waldo_xyz_tdoa_nlm(int bsize, waldo_xyzt_t *base, waldo_real_t velocity,
		       waldo_xyz_t *start, waldo_xyz_t *pos,
		       int iterations, waldo_real_t epsilon);

/**
 * Solving TDOA using Nelder-Mead optimization algorithm.
 *
 * @note Here Z is supposed to be known be other mean.
 */
int waldo_xyZ_tdoa_nlm(int bsize, waldo_xyzt_t *base, waldo_real_t velocity,
		       waldo_xyz_t *start, waldo_xyz_t *pos,
		       int iterations, waldo_real_t epsilon);

/**
 * Solving TDOA using CHAN closed form algorithm.
 */
int waldo_xyz_tdoa_chan_ab(int bsize, waldo_xyzt_t *base, waldo_real_t velocity,
			   waldo_xyz_t *a_far, waldo_xyz_t *a_near,
			   waldo_xyz_t *b_near, waldo_xyz_t *b_far);

/**
 * Solving TDOA using CHAN closed form algorithm.
 */
int waldo_xyz_tdoa_chan(int bsize, waldo_xyzt_t *base, waldo_real_t velocity,
			waldo_xyz_t *near, waldo_xyz_t *far);

#endif
