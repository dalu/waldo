/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__RANDOM__H
#define __WALDO__RANDOM__H

#include <stdlib.h>
#include <stdint.h>

#include <waldo.h>
#include <waldo/coordinates.h>


static inline int32_t
waldo_random_int32(void)
{
    // mrand48 : range [-2**31, 2**31-1]
    return (int32_t)mrand48();
}

static inline uint32_t
waldo_random_uint32(void)
{
    // lrand48 : range [0, 2**31-1]
    return (uint32_t)lrand48();
}

static inline int
waldo_random_int(void)
{
    return waldo_random_int32();
}

static inline unsigned int
waldo_random_uint(void)
{
    return waldo_random_uint32();
}

/**
 * Random number [0.0, 1.0)
 */
static inline double
waldo_random_double(void)
{
    return drand48();
}

#if WALDO_REAL_SIZE == WALDO_REAL_F64
#define waldo_random_real waldo_random_double
#endif


void waldo_random_xy_circle(waldo_xy_t *xy, waldo_real_t radius);
void waldo_random_xy_disc(waldo_xy_t *xy, waldo_real_t radius);
void waldo_random_xy_square(waldo_xy_t *xy, waldo_real_t side);
void waldo_random_xyz_sphere(waldo_xyz_t *xyz, waldo_real_t radius);
void waldo_random_xyz_ball(waldo_xyz_t *xyz, waldo_real_t radius);
void waldo_random_xyz_hollow_ball(waldo_xyz_t *xyz, waldo_real_t r1, waldo_real_t r2);
void waldo_random_xyz_cube(waldo_xyz_t *xyz, waldo_real_t side);


#endif
