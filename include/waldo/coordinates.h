
/*
 * Copyright (c) 2022-2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__COORDINATE__H
#define __WALDO__COORDINATE__H

#include <math.h>
#include <stdbool.h>

#define _WALDO_COORDINATE_COPY(type)					\
    static inline void							\
    waldo_##type##_copy(waldo_##type##_t *src, waldo_##type##_t *dst) {	\
	for (int i = 0 ; i < (int)WALDO_ARRAY_SIZE(src->axis) ; i++)	\
	    dst->axis[i] = src->axis[i];				\
    }

#define _WALDO_COORDINATE_ISNAN(type)					\
    static inline bool							\
    waldo_##type##_isnan(waldo_##type##_t *v) {				\
	for (int i = 0 ; i < (int)WALDO_ARRAY_SIZE(v->axis) ; i++)	\
	    if (isnan(v->axis[i])) return true;				\
	return false;							\
    }


//== XY ================================================================

#ifndef WALDO_PRI_XY_REAL
#define WALDO_PRI_XY_REAL WALDO_PRI_REAL
#endif

#ifndef WALDO_PRI_XY
#define WALDO_PRI_XY					\
    "<" WALDO_PRI_XY_REAL ", " WALDO_PRI_XY_REAL ">"
#endif

#ifndef WALDO_PRI_XY_CSV
#define WALDO_PRI_XY_CSV				\
    WALDO_PRI_XY_REAL ", " WALDO_PRI_XY_REAL 
#endif

#define WALDO_ARG_XY(xy) (xy).x, (xy).y

typedef union waldo_xy {
    struct {
	waldo_real_t x;
	waldo_real_t y;
    };
    waldo_real_t axis[2];	
} waldo_xy_t;

typedef union waldo_xyt {
    struct {
	waldo_real_t x;
	waldo_real_t y;
	waldo_real_t t;
    };
    waldo_real_t axis[3];
    waldo_xy_t xy;
} waldo_xyt_t;

#define WALDO_XY_NAN   { NAN, NAN }
#define WALDO_XYT_NAN  { NAN, NAN, NAN }

_WALDO_COORDINATE_COPY(xy)
_WALDO_COORDINATE_COPY(xyt)
_WALDO_COORDINATE_ISNAN(xy)
_WALDO_COORDINATE_ISNAN(xyt)

static inline waldo_real_t
waldo_xy_dot(waldo_xy_t *a, waldo_xy_t *b) {
    return a->x * b->x + a->y * b->y;
}

static inline waldo_real_t
waldo_xy_norm(waldo_xy_t *a) {
    return sqrt(waldo_xy_dot(a, a));
}

static inline waldo_real_t
waldo_xy_dist(waldo_xy_t *a, waldo_xy_t *b) {
    return sqrt(pow(a->x - b->x, 2) +
		pow(a->y - b->y, 2));
}

static inline void
waldo_xy_axpy(waldo_xy_t *r, waldo_xy_t *x, waldo_real_t alpha, waldo_xy_t *y)
{
    r->x = alpha * x->x + y->x;
    r->y = alpha * x->y + y->y;
}

static inline void
waldo_xy_scale(waldo_xy_t *r, waldo_xy_t *a, waldo_real_t scale) {
    r->x = a->x * scale;
    r->y = a->y * scale;
}

static inline void
waldo_xy_add(waldo_xy_t *r, waldo_xy_t *a, waldo_xy_t *b) {
    r->x = a->x + b->x;
    r->y = a->y + b->y;
}

static inline void
waldo_xy_sub(waldo_xy_t *r, waldo_xy_t *a, waldo_xy_t *b) {
    r->x = a->x - b->x;
    r->y = a->y - b->y;
}

static inline void
waldo_xy_normalize(waldo_xy_t *r, waldo_xy_t *a) {
    waldo_xy_scale(r, a, 1.0 / waldo_xy_norm(a));
}



//== XYZ ===============================================================

#ifndef WALDO_PRI_XYZ_REAL
#define WALDO_PRI_XYZ_REAL WALDO_PRI_REAL
#endif

#ifndef WALDO_PRI_XYZ
#define WALDO_PRI_XYZ							\
    "<" WALDO_PRI_XYZ_REAL ", " WALDO_PRI_XYZ_REAL ", " WALDO_PRI_XYZ_REAL ">"
#endif

#ifndef WALDO_PRI_XYZ_CSV
#define WALDO_PRI_XYZ_CSV						\
    WALDO_PRI_XYZ_REAL ", " WALDO_PRI_XYZ_REAL ", " WALDO_PRI_XYZ_REAL
#endif

#define WALDO_ARG_XYZ(xyz) (xyz).x, (xyz).y, (xyz).z

typedef union waldo_xyz {
    struct {
	waldo_real_t x;
	waldo_real_t y;
	waldo_real_t z;
    };
    waldo_real_t axis[3];
    waldo_xy_t xy;
} waldo_xyz_t;


typedef union waldo_xyzt {
    struct {
	waldo_real_t x;
	waldo_real_t y;
	waldo_real_t z;
	waldo_real_t t;
    };
    waldo_real_t axis[4];
    waldo_xyz_t xyz;
    waldo_xy_t  xy;
} waldo_xyzt_t;


#define WALDO_XYZ_NAN   { NAN, NAN, NAN }
#define WALDO_XYZT_NAN  { NAN, NAN, NAN, NAN }

_WALDO_COORDINATE_COPY(xyz);
_WALDO_COORDINATE_COPY(xyzt);
_WALDO_COORDINATE_ISNAN(xyz);
_WALDO_COORDINATE_ISNAN(xyzt);

static inline waldo_real_t
waldo_xyz_dot(waldo_xyz_t *a, waldo_xyz_t *b) {
    return a->x * b->x + a->y * b->y + a->z * b->z;
}

static inline waldo_real_t
waldo_xyz_norm(waldo_xyz_t *a) {
    return sqrt(waldo_xyz_dot(a, a));
}

static inline void
waldo_xyz_axpy(waldo_xyz_t *r, waldo_xyz_t *x, waldo_real_t alpha, waldo_xyz_t *y)
{
    r->x = alpha * x->x + y->x;
    r->y = alpha * x->y + y->y;
    r->z = alpha * x->z + y->z;
}

static inline waldo_real_t
waldo_xyz_dist(waldo_xyz_t *a, waldo_xyz_t *b) {
    return sqrt(pow(a->x - b->x, 2) +
		pow(a->y - b->y, 2) +
		pow(a->z - b->z, 2));
}

static inline void
waldo_xyz_scale(waldo_xyz_t *r, waldo_xyz_t *a, waldo_real_t scale) {
    r->x = a->x * scale;
    r->y = a->y * scale;
    r->z = a->z * scale;
}

static inline void
waldo_xyz_add(waldo_xyz_t *r, waldo_xyz_t *a, waldo_xyz_t *b) {
    r->x = a->x + b->x;
    r->y = a->y + b->y;
    r->z = a->z + b->z;
}

static inline void
waldo_xyz_sub(waldo_xyz_t *r, waldo_xyz_t *a, waldo_xyz_t *b) {
    r->x = a->x - b->x;
    r->y = a->y - b->y;
    r->z = a->z - b->z;
}

static inline void
waldo_xyz_cross(waldo_xyz_t *r, waldo_xyz_t *a, waldo_xyz_t *b) {
    r->x = a->y * b->z - a->z * b->y;
    r->y = a->z * b->x - a->x * b->z;
    r->z = a->x * b->y - a->y * b->x;
}

static inline void
waldo_xyz_normalize(waldo_xyz_t *r, waldo_xyz_t *a) {
    waldo_xyz_scale(r, a, 1.0 / waldo_xyz_norm(a));
}



//== LL ================================================================

typedef union waldo_ll {
    struct {
	waldo_real_t lat;
	waldo_real_t lon;
    };
    waldo_real_t axis[2];	
} waldo_ll_t;


typedef union waldo_llt {
    struct {
	waldo_real_t lat;
	waldo_real_t lon;
	waldo_real_t t;
    };
    waldo_real_t axis[3];
    waldo_ll_t ll;
} waldo_llt_t;

#define WALDO_LL_NAN   { NAN, NAN }
#define WALDO_LLT_NAN  { NAN, NAN, NAN }

_WALDO_COORDINATE_COPY(ll);
_WALDO_COORDINATE_COPY(llt);
_WALDO_COORDINATE_ISNAN(ll);
_WALDO_COORDINATE_ISNAN(llt);


//== LLZ ================================================================

typedef union waldo_llz {
    struct {
	waldo_real_t lat;
	waldo_real_t lon;
	waldo_real_t z;
    };
    waldo_real_t axis[3];
    waldo_ll_t ll;
} waldo_llz_t;

typedef union waldo_llzt {
    struct {
	waldo_real_t lat;
	waldo_real_t lon;
	waldo_real_t z;
	waldo_real_t t;
    };
    waldo_real_t axis[4];
    waldo_llz_t llz;
    waldo_ll_t  ll;
} waldo_llzt_t;

#define WALDO_LLZ_NAN   { NAN, NAN, NAN }
#define WALDO_LLZT_NAN  { NAN, NAN, NAN, NAN }

_WALDO_COORDINATE_COPY(llz);
_WALDO_COORDINATE_COPY(llzt);
_WALDO_COORDINATE_ISNAN(llz);
_WALDO_COORDINATE_ISNAN(llzt);

#endif
