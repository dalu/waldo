/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__HAVERSINE__H
#define __WALDO__HAVERSINE__H

double
waldo_haversine_inverse(double radius,
			double λ1, double φ1,
			double λ2, double φ2);
     
void waldo_haversine_direct(double radius,
			    double λ1, double φ1, double α1, double s,
			    double *_λ2, double *_φ2);
        
double waldo_haversine_initial_bearing(double λ1, double φ1,
				       double λ2, double φ2);
        
double waldo_haversine_final_bearing(double λ1, double φ1,
				     double λ2, double φ2);
#endif
