/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__ELLIPSOID__H
#define __WALDO__ELLIPSOID__H

/**
 * Ellipsoid structure definition.
 */
typedef struct waldo_ellipsoid {
    double a;   // Major semi-axis
    double b;   // Minor semi-axis
    double f;   // Flattening
    double e2;  // Eccentricity^2
} waldo_ellipsoid_t;


#define WALDO_ELLIPSOID_WGS72           0
#define WALDO_ELLIPSOID_WGS84  		1
#define WALDO_ELLIPSOID_GRS80		2
#define WALDO_ELLIPSOID_PZ90		3
#define WALDO_ELLIPSOID_IERS		4
#define WALDO_ELLIPSOID_KRSTY		5
#define WALDO_ELLIPSOID_MAXCOUNT	6


/**
 * Compute eccentricity from semi-axes a and b.
 */
#define WALDO_ELLIPSOID_EXCENTRICITY2(a, b)				\
    (((a) * (a) - (b) * (b)) / ((a) * (a)))


/**
 * Initialise ellipsoid structure from major semi-axis and inverse flattening.
 */
#define WALDO_ELLIPSOID_INIT(mjsa, ifltn)				\
    {   .a  = mjsa,							\
	.b  = mjsa - mjsa / ifltn,					\
        .f  = 1.0 / ifltn,						\
	.e2  = WALDO_ELLIPSOID_EXCENTRICITY2(mjsa, mjsa - mjsa / ifltn),\
    }



#endif
