/*
 * Copyright (c) 2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__TOA__H
#define __WALDO__TOA__H


/**
 * Solving TOA using Nelder-Mead optimization algorithm.
 */
int waldo_xyz_toa_nlm(int bsize, waldo_xyzt_t *base, waldo_real_t velocity,
		      waldo_xyz_t *start, waldo_xyz_t *pos,
		      int iterations, waldo_real_t epsilon);

#endif
