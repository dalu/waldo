/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/*
    # Convention:
    # x = λ = longitude
    # y = φ = latitude
    # 1     = from
    # 2     = to
*/

#ifndef __WALDO__VINCENTY__H
#define __WALDO__VINCENTY__H

#include <stdbool.h>

#ifndef WALDO_VINCENTY_LIMIT
#define WALDO_VINCENTY_LIMIT  		400
#endif

#ifndef WALDO_VINCENTY_EPSILON
#define WALDO_VINCENTY_EPSILON   	1e-12
#endif

bool
waldo_vincenty_direct(waldo_ellipsoid_t *el,
		      double λ1, double φ1, double α1, double s,
		      double *λ2, double *φ2, double *α2);

bool
waldo_vincenty_inverse(waldo_ellipsoid_t *el,
		       double λ1, double φ1,
		       double λ2, double φ2,
		       double *s, double *α1, double *α2);

#endif
