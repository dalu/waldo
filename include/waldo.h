/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __WALDO__H
#define __WALDO__H
/*
  static_assert(sizeof(double) == 8, "double is not 64-bit");
  static_assert(__ARM_FP & 0x04, "no 32-bit float FPU support");
  static_assert(__ARM_FP & 0x08, "no 64-bit float FPU support");
*/

#include <math.h>
#include <stdint.h>
#include <stdbool.h>

#include <assert.h>
#define WALDO_ASSERT(cond)					\
    assert(cond)

#define WALDO_ARRAY_SIZE(a) 					\
    (sizeof(a) / sizeof(a[0]))

#define WALDO_SWAP(x, y)					\
    do {							\
	typeof(x) tmp = x;					\
	x = y;							\
	y = tmp;						\
    } while (0)

#define WALDO_XSTR(s) WALDO_STR(s)
#define WALDO_STR(s) #s

#define WALDO_MIN(a,b) ((a) < (b) ? (a) : (b))
#define WALDO_MAX(a,b) ((a) < (b) ? (b) : (a))
#define WALDO_CLAMP(v, min, max) WALDO_MIN(max, WALDO_MAX(min, v)) 




#define _WALDO_FLEXARRAY_ALLOCSIZE(type, field, size)			\
    WALDO_MAX(sizeof(type),						\
	       offsetof(type, field) + sizeof((type){ 0 }.field[0]) * size)


#include <errno.h>

#define WALDO_OK                      0
#define WALDO_ERR_NOT_SUPPORTED       (-EOPNOTSUPP)
#define WALDO_ERR_NO_MEMORY           (-ENOMEM    )
#define WALDO_ERR_INVALID_PARAMETER   (-EINVAL    )
#define WALDO_ERR_NOT_YET_IMPLEMENTED (-ENOSYS    )
#define WALDO_ERR_MATH                (-EDOM      )
#define WALDO_ERR_UNKNOWN             (-666       )

#define WALDO_RETURN_IF_ERROR(e)		\
    do {					\
	int error = e;				\
	if (error < 0)				\
	    return error;			\
    } while(0)




#define WALDO_EPSILON    1e-8
#define WALDO_ITERATIONS 2000


#define WALDO_REAL_F32 32
#define WALDO_REAL_F64 64

#define WALDO_REAL_SIZE WALDO_REAL_F64
typedef double waldo_real_t;

#define WALDO_REAL(v) ((waldo_real_t) (v))


#ifndef WALDO_PRI_REAL
#define WALDO_PRI_REAL "%f"
#endif



char *waldo_version(void);


static inline double
waldo_clamp(double v, double min, double max) {
    if (!isnan(min) && (v < min))
	return min;
    if (!isnan(max) && (v > max))
	return max;
    return v;
}


static inline bool
waldo_is_zero(waldo_real_t v) {
    return fabs(v) < WALDO_EPSILON;
}

#include <waldo/coordinates.h>
#include <waldo/ellipsoid.h>
#include <waldo/vincenty.h>


#endif
