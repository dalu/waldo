# waldo

Localization library suitable for embedded software.

# Build
You can define the following:

| Name                    | Type      | Description                    |
|-------------------------|-----------|--------------------------------|
| BUILD_SHARED_LIBS:BOOL  | Boolean   | Build shared or static library |
| CMAKE_INSTALL_PREFIX    | Directory | Define installation prefix     |
| OPTIMIZE                | Boolean   | Build with optimization flag   |
| SANITY_CHECK            | Boolean   | Enable code assertions         |


For example building and installing (as a shared library)
~~~
cmake -DBUILD_SHARED_LIBS:BOOL=ON -DCMAKE_INSTALL_PREFIX=/opt/waldo -B build
make -C build
make -C build install
~~~


# Credits
* TDOA CHAN: https://programmersought.com/article/336610767583/
* Nelder-Mead: https://github.com/matteotiziano/nelder-mead
* Vincenty & Haversine: https://www.movable-type.co.uk/scripts/latlong.html
