#ifndef __WORLD__H
#define __WORLD__H

#include <waldo.h>

#ifndef WORLD_PROPAGATION_SPEED
#define WORLD_PROPAGATION_SPEED 299702547.0
#endif

typedef struct world {
    struct {
	int antena_error;
	double clock_precision;
    } uwb;
} world_t;

double world_uwb_propagation_time(world_t *w, waldo_xyz_t *a, waldo_xyz_t *b);

#endif
