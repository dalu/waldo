#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include <waldo.h>
#include <waldo/random.h>
#include <waldo/coordinates.h>

#include "world.h"
#include "uav.h"

world_t world_default = {
    .uwb.antena_error    = 0,
    .uwb.clock_precision = 64e9,
};


int main(int argc, char *argv[]) {
     extern char *optarg;
     extern int optind;
     int c;
     int shape = 6;
     double radius = 10.0;
     
     static struct option longopts[] = {
	 { "seed",       required_argument, NULL, 'S' },
	 { "shape",      required_argument, NULL, 's' },
	 { "radius",     required_argument, NULL, 'r' },
	 { "help",       no_argument,       NULL, '?' },
	 { 0 }
     };

     while ((c = getopt_long(argc, argv, "S:s:", longopts, NULL)) != -1) {
	 switch (c) {
	 case 'S': {
	     long long number = strtoll(optarg,  NULL, 0);
	     unsigned short seed[3] = { (number >>  0) & 0xFFFF,
					(number >> 16) & 0xFFFF,
					(number >> 32) & 0xFFFF };
	     seed48(seed);
	     break;
	 }
	 case 's':
	     shape = atoi(optarg);
	     if ((shape < 4) || (shape > 6)) {
		 fprintf(stderr, "only value 4, 5, or 6 is allowed\n");
		 exit(1);
	     }
	     break;
	 case 'r':
	     radius = atof(optarg);
	     if (radius <= 0) {
		 fprintf(stderr, "radius need to be greater than 0\n");
		 exit(1);
	     }
	     break;
	 default:
	     printf("%s [-s|--shape=4|5|6] [-S|--seed=integer] [-r|--radius=float]\n", argv[0]);
	     exit(0);
	 }
     }
    argc -= optind;
    argv += optind;
	    
	

    uav_config_t uav_config = { 0 };

    switch(shape) {
    case 4: UAV_CONFIG_SET_ANTENNA(uav_config, 4); break;
    case 5: UAV_CONFIG_SET_ANTENNA(uav_config, 5); break;
    case 6: UAV_CONFIG_SET_ANTENNA(uav_config, 6); break;
    }

    waldo_xyz_t real;
    waldo_random_xyz_ball(&real, radius);

    uav_position_t func[] = {
	uav_xyz_position_toa_nlm,
	uav_xyz_position_tdoa_chan,
	uav_xyz_position_tdoa_nlm,
	uav_xyZ_position_tdoa_nlm,
    };    
	
    printf("Receivers:\n");
    for (int i = 0 ; i < uav_config.antenna.count ; i++) {
	printf("  [%d] = %6.2f, %6.2f, %6.2f\n", i,
	       WALDO_ARG_XYZ(uav_config.antenna.base[i]));
    }

    
    printf("\n");
    printf("Position : " WALDO_PRI_XYZ "\n", WALDO_ARG_XYZ(real));

    printf("\n");
    printf("Estimations:\n");

    for (int i = 0 ; i < WALDO_ARRAY_SIZE(func) ; i++) {
	waldo_xyz_t estimated = WALDO_XYZ_NAN;
	func[i](&world_default, &uav_config, &real, &estimated);
	waldo_real_t err = waldo_xyz_dist(&real, &estimated);
	printf("%-16s : " WALDO_PRI_XYZ_CSV " (err = %8.4f)\n",
	       uav_get_position_name(func[i]),
	       WALDO_ARG_XYZ(estimated), err);
    }
    
    exit(0);
}
