#include <waldo.h>
#include <waldo/random.h>

#include "world.h"

double
world_uwb_propagation_time(world_t *w, waldo_xyz_t *a, waldo_xyz_t *b) {
    double t = waldo_xyz_dist(a, b) / WORLD_PROPAGATION_SPEED;
    double e = 0.0;
    if (w->uwb.antena_error > 0) {
	e  = w->uwb.antena_error;
	e *= (waldo_random_double() - 0.5) * 2.0;
    }
    return floor(t * w->uwb.clock_precision + e) / w->uwb.clock_precision;
}
