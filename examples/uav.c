#include <waldo.h>
#include <waldo/random.h>
#include <waldo/coordinates.h>
#include <waldo/tdoa.h>
#include <waldo/toa.h>

#include "world.h"
#include "uav.h"


static void
uav_init_anchors(waldo_xyzt_t *base,
		 world_t *w, uav_config_t *cfg, waldo_xyz_t *real)
{
    for (int i = 0 ; i < cfg->antenna.count ; i++) {
	base[i].xyz = cfg->antenna.base[i];
	base[i].t   = world_uwb_propagation_time(w, &base[i].xyz, real);
    }
}

static void
uav_init_estimated_search_start(waldo_xyz_t *start, int bsize, waldo_xyzt_t *base)
{
    waldo_xyz_t bary = { 0 };
    for (int i = 0 ; i < bsize ; i++) {
	waldo_xyz_t weighted;
	waldo_xyz_scale(&weighted, &base[i].xyz, 1.0 / base[i].t);
	waldo_xyz_add(&bary, &bary, &weighted);
    }
    start->x = bary.x > 0.0 ? 10.0 : -10.0;
    start->y = bary.y > 0.0 ? 10.0 : -10.0;
    start->z = bary.z > 0.0 ? 10.0 : -10.0;
}



int
uav_xyz_position_tdoa_chan(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated) {
    int bsize = cfg->antenna.count;

    waldo_xyzt_t base[bsize];
    uav_init_anchors(base, w, cfg, real);

    waldo_xyz_t a_near, a_far, b_near, b_far;

    int n = waldo_xyz_tdoa_chan_ab(bsize, base, WORLD_PROPAGATION_SPEED,
				   &a_near, &a_far, &b_near, &b_far);
    if (n < 0) return n;

    waldo_xyz_t *best     = NULL;
    waldo_real_t best_err = 1.0e66;
    struct {
	waldo_xyz_t *pos;
	waldo_real_t err;
    } lookup[] = { { .pos = &a_far  },
		   { .pos = &a_near },
		   { .pos = &b_near },
		   { .pos = &b_far  } };

    for (int i = 0 ; i < n ; i++) {
	lookup[i].err = waldo_xyz_dist(real, lookup[i].pos);
	if (lookup[i].err < best_err) {
	    best_err = lookup[i].err;
	    best = lookup[i].pos;
	}
    }
    *estimated = *best;

#if DEBUG
    printf("best = %f\n",best_err);
    printf(" R: <" WALDO_PRI_XYZ_CSV ">\n"
	   " A: <" WALDO_PRI_XYZ_CSV "> = %f\n"
	   " A: <" WALDO_PRI_XYZ_CSV "> = %f\n"
	   " B: <" WALDO_PRI_XYZ_CSV "> = %f\n"
	   " B: <" WALDO_PRI_XYZ_CSV "> = %f\n"
	   "---" "\n",
	   WALDO_ARG_XYZ(*real),
	   WALDO_ARG_XYZ(a_far), lookup[0].err,
	   WALDO_ARG_XYZ(a_near), lookup[1].err,
	   WALDO_ARG_XYZ(b_near), lookup[2].err,
	   WALDO_ARG_XYZ(b_far), lookup[3].err
	   );

    
    waldo_xyz_tdoa_chan(bsize, base, WORLD_PROPAGATION_SPEED, &a_near, estimated);
    
    printf(" E: <" WALDO_PRI_XYZ_CSV ">\n",
	   WALDO_ARG_XYZ(*estimated));
#endif
    

    return n;
}


int
uav_xyZ_position_tdoa_nlm(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated) {

    int bsize = cfg->antenna.count;

    waldo_xyzt_t base[bsize];
    uav_init_anchors(base, w, cfg, real);

    waldo_xyz_t  start;
    uav_init_estimated_search_start(&start, bsize, base);
    start.z = real->z;
    
    return waldo_xyZ_tdoa_nlm(bsize, base, WORLD_PROPAGATION_SPEED,
			      &start, estimated, 1200, 1e-10);
}



int
uav_xyz_position_toa_nlm(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated) {

    int bsize = cfg->antenna.count;

    waldo_xyzt_t base[bsize];
    uav_init_anchors(base, w, cfg, real);

    waldo_xyz_t  start;
    uav_init_estimated_search_start(&start, bsize, base);

    return waldo_xyz_toa_nlm(bsize, base, WORLD_PROPAGATION_SPEED,
			     &start, estimated, 1200, 1e-10);
}

int
uav_xyz_position_tdoa_nlm(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated) {

    int bsize = cfg->antenna.count;

    waldo_xyzt_t base[bsize];
    uav_init_anchors(base, w, cfg, real);

    waldo_xyz_t  start;
    uav_init_estimated_search_start(&start, bsize, base);
    
    return waldo_xyz_tdoa_nlm(bsize, base, WORLD_PROPAGATION_SPEED,
			      &start, estimated, 1200, 1e-10);
}


char *uav_get_position_name(uav_position_t func) {
    static struct {
	uav_position_t func;
	char *name;
    } uav_position[] = {
	{ uav_xyz_position_toa_nlm,   "ToA  xyz NLM"  },
	{ uav_xyz_position_tdoa_chan, "TDoA xyz CHAN" },
	{ uav_xyz_position_tdoa_nlm,  "TDoA xyz NLM"  },
	{ uav_xyZ_position_tdoa_nlm,  "TDoA xyZ NLM"  },
    };

    for (int i = 0 ; i < WALDO_ARRAY_SIZE(uav_position) ; i++) {
	if (uav_position[i].func == func)
	    return uav_position[i].name;
    }

    return "unknown";
};
