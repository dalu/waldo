#ifndef __UAV__H
#define __UAV__H

#include <waldo.h>

#define UAV_CONFIG_ANTENNA_MAX 6


#define UAV_CONFIG_ANTENNA_4						\
    {   .count   = 4,							\
	.base[0] = { .x =  sqrt(8.0/9.0),                     .z = -1.0/3.0 }, \
	.base[1] = { .x = -sqrt(2.0/9.0), .y=  sqrt(2.0/3.0), .z = -1.0/3.0 }, \
	.base[2] = { .x = -sqrt(2.0/9.0), .y= -sqrt(2.0/3.0), .z = -1.0/3.0 }, \
	.base[3] = {                                          .z =  1.0     }, \
    }

#define UAV_CONFIG_ANTENNA_5						\
    {   .count     = 5,							\
	.base[0].z =  1.0,						\
	.base[1].z = -1.0,						\
	.base[2].x =  1.0,						\
	.base[3]   = { .x = -0.5, .y =  sqrt(3.0) / 2.0 },		\
	.base[4]   = { .x = -0.5, .y = -sqrt(3.0) / 2.0 },		\
    }

#define UAV_CONFIG_ANTENNA_6			\
    {   .count     = 6,				\
	.base[0].x =  1.0,			\
	.base[1].x = -1.0,			\
	.base[2].y =  1.0,			\
	.base[3].y = -1.0,			\
	.base[4].z =  1.0,			\
	.base[5].z = -1.0,			\
    }

#define UAV_CONFIG_SET_ANTENNA(uav_config, v)	\
    (uav_config).antenna = (typeof((uav_config).antenna)) UAV_CONFIG_ANTENNA_##v


typedef struct uav_config {
    struct {
	int count;
	waldo_xyz_t base[UAV_CONFIG_ANTENNA_MAX];
    } antenna;
} uav_config_t;


typedef int (*uav_position_t)(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
			      waldo_xyz_t *estimated);



int uav_xyz_position_toa_nlm(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated);

int uav_xyz_position_tdoa_nlm(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated);

int uav_xyZ_position_tdoa_nlm(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated);

int uav_xyz_position_tdoa_nlm2(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated);

int uav_xyZ_position_tdoa_nlm2(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated);

int uav_xyz_position_tdoa_chan(world_t *w, uav_config_t *cfg, waldo_xyz_t *real,
		 waldo_xyz_t *estimated);


char *uav_get_position_name(uav_position_t func);


#endif
