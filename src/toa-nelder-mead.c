#include <stdlib.h>
#include <waldo.h>
#include <waldo/toa.h>
#include <waldo/optimizer.h>



struct _waldo_xyz_toa_base {
    waldo_xyz_t  pos;
    waldo_real_t distance;
};

struct _waldo_xyz_toa {
    int count;
    struct _waldo_xyz_toa_base *base;
};

static waldo_real_t
_waldo_cost_toa(int n, waldo_real_t *e, const void *args)
{
    WALDO_ASSERT(n == 3);
    
    waldo_xyz_t                 *pos = (waldo_xyz_t *)e;
    const struct _waldo_xyz_toa *toa = args;
    waldo_real_t                 r   = 0.0;

    for (int i = 0 ; i < toa->count ; i++) {
	struct _waldo_xyz_toa_base *base = &toa->base[i];
	r += pow(waldo_xyz_dist(&base->pos, pos) - base->distance, 2);
    }

    return r;
}


int waldo_xyz_toa_nlm(int size, waldo_xyzt_t *bases, waldo_real_t velocity,
		       waldo_xyz_t *start, waldo_xyz_t *pos,
		       int iterations, double epsilon) {
    struct _waldo_xyz_toa_base base[size];
    for (int i = 0 ; i < size ; i++) {
	base[i] = (struct _waldo_xyz_toa_base) {
	    .pos.x    = bases[i].x,
	    .pos.y    = bases[i].y,
	    .pos.z    = bases[i].z,
	    .distance = bases[i].t * velocity
	};
    }
    struct _waldo_xyz_toa toa = {
	.count = size,
	.base  = base
    };

    waldo_optimizer_limits_t limits = {
	.epsilon_x   = epsilon,
	.epsilon_fx  = epsilon,
	.max_iter    = iterations,
    };
    waldo_optimizer_stats_t stats;
    
    return waldo_optimizer_nelder_mead(3, _waldo_cost_toa, &toa,
				       start->axis, pos->axis, &limits, NULL);
}


