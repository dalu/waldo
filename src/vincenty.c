#include <math.h>
#include <stdint.h>
#include <stdbool.h>

#include "waldo.h"
#include "waldo/ellipsoid.h"
#include "waldo/vincenty.h"

bool
waldo_vincenty_inverse(waldo_ellipsoid_t *el,
		       double λ1, double φ1,
		       double λ2, double φ2,
		       double *s, double *α1, double *α2)
{
    double l       = λ2 - λ1;
    double tanU1   = (1.0 - el->f) * tan(φ1);
    double cosU1   = 1.0 / sqrt(1.0 + tanU1*tanU1);
    double sinU1   = tanU1 * cosU1;
    double tanU2   = (1.0 - el->f) * tan(φ2);
    double cosU2   = 1.0 / sqrt(1.0 + tanU2*tanU2);
    double sinU2   = tanU2 * cosU2;

    double λ       = l;
    double λʹ      = 0.0;
    int    i       = 0;

    double sinλ, cosλ, sinσ, cosσ, σ, sinα, cos²α, cos2σm, c;

    for (;;) {
	sinλ   = sin(λ);
	cosλ   = cos(λ);
	sinσ   = sqrt((cosU2*sinλ)*(cosU2*sinλ) +
		      (cosU1*sinU2-sinU1*cosU2*cosλ)*
		      (cosU1*sinU2-sinU1*cosU2*cosλ));
	cosσ   = sinU1*sinU2 + cosU1*cosU2*cosλ;
	σ      = atan2(sinσ, cosσ);
	sinα   = cosU1 * cosU2 * sinλ / sinσ;
	cos²α  = 1.0 - sinα*sinα;
	cos2σm = cos²α > 1e-12 ? cosσ - 2.0*sinU1*sinU2/cos²α : 0;
	c      = el->f/16.0*cos²α*(4.0+el->f*(4.0-3.0*cos²α));

	λʹ     = λ;
	λ      = l + (1.0-c) * el->f * sinα *
	           (σ + c*sinσ*(cos2σm+c*cosσ*(-1.0+2.0*cos2σm*cos2σm)));

	if (++i        >= WALDO_VINCENTY_LIMIT  ) return false;
	if (fabs(λ-λʹ) <= WALDO_VINCENTY_EPSILON) break;
    }

    double u² = cos²α * (el->a*el->a - el->b*el->b) / (el->b*el->b);
    double A = 1 + u²/16384.0*(4096.0+u²*(-768.0+u²*(320.0-175.0*u²)));
    double B = u²/1024.0 * (256+u²*(-128+u²*(74-47*u²)));
    double Δσ = B*sinσ*(cos2σm+B/4.0*(cosσ*(-1.0+2.0*cos2σm*cos2σm)-
		   B/6.0*cos2σm*(-3.0+4.0*sinσ*sinσ)*(-3.0+4.0*cos2σm*cos2σm)));

    *s  = el->b*A*(σ-Δσ);
    *α1 = atan2(cosU2*sinλ,  cosU1*sinU2-sinU1*cosU2*cosλ);
    *α2 = atan2(cosU1*sinλ, -sinU1*cosU2+cosU1*sinU2*cosλ);
    return true;
}


bool
waldo_vincenty_direct(waldo_ellipsoid_t *el,
		      double λ1, double φ1, double α1, double s,
		      double *λ2, double *φ2, double *α2)
{
    double sinα1   = sin(α1);
    double cosα1   = cos(α1);
    double tanU1   = (1.0 - el->f) * tan(φ1);
    double cosU1   = 1.0 / sqrt(1.0 + tanU1 * tanU1);
    double sinU1   = tanU1 * cosU1;
    double σ1      = atan2(tanU1, cosα1);
    double sinα    = cosU1 * sinα1;
    double cos²α   = 1.0 - sinα*sinα;
    double u²      = cos²α * (el->a*el->a - el->b*el->b) / (el->b*el->b);
    double A       = 1.0 + u²/16384.0*(4096.0+u²*(-768.0+u²*(320.0-175.0*u²)));
    double B       = u²/1024.0*(256.0+u²*(-128.0+u²*(74.0-47.0*u²)));

    double σ       = s / (el->b * A);
    double σʹ      = 0.0;
    int    i       = 0;
    
    double cos2σm, sinσ, cosσ, Δσ;
    
    for (;;) {
	cos2σm = cos(2.0*σ1 + σ);
	sinσ   = sin(σ);
	cosσ   = cos(σ);
	Δσ     = B*sinσ*(cos2σm+B/4.0*(cosσ*(-1.0+2.0*cos2σm*cos2σm)-
		   B/6.0*cos2σm*(-3.0+4.0*sinσ*sinσ)*(-3.0+4.0*cos2σm*cos2σm)));

	σʹ     = σ;
	σ      = s / (el->b*A) + Δσ;
        
	if (++i        >= WALDO_VINCENTY_LIMIT  ) return false;
	if (fabs(σ-σʹ) <= WALDO_VINCENTY_EPSILON) break;
    }

    double x   = sinU1*sinσ - cosU1*cosσ*cosα1;
    double λ   = atan2(sinσ*sinα1, cosU1*cosσ - sinU1*sinσ*cosα1);
    double C   = el->f/16.0*cos²α*(4.0+el->f*(4.0-3.0*cos²α));
    double L   = λ - (1.0-C) * el->f * sinα *
	           (σ + C*sinσ*(cos2σm+C*cosσ*(-1.0+2.0*cos2σm*cos2σm))); 

    *φ2 = atan2(sinU1*cosσ + cosU1*sinσ*cosα1,
		(1.0-el->f)*sqrt(sinα*sinα + x*x));
    *λ2 = λ1 + L;
    *α2 = atan2(sinα, -x);
    return true;
}

