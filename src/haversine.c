#include <math.h>
#include <stdint.h>

#include "waldo.h"
#include "waldo/haversine.h"



double
waldo_haversine_inverse(double radius,
			double λ1, double φ1,
			double λ2, double φ2) {
    double Δλ = λ2 - λ1;
    double Δφ = φ2 - φ1;
    double h  =  pow(sin(Δφ/2.0),2) + cos(φ1)*cos(φ2)*pow(sin(Δλ/2.0),2);
    double Δσ = 2.0 * atan2(sqrt(h), sqrt(1.0-h));
    return radius * Δσ;
}
     
void
waldo_haversine_direct(double radius,
		       double λ1, double φ1, double α1, double s,
		       double *_λ2, double *_φ2) {
    double d  = s / radius;
    double φ2 = asin(sin(φ1)*cos(d) + cos(φ1)*sin(d)*cos(α1));
    double Δλ = atan2(sin(α1)*sin(d)*cos(φ1), cos(d)-sin(φ1)*sin(φ2));
    double λ2 = λ1 + Δλ;

    *_λ2 = λ2;
    *_φ2 = φ2;
}
        
double
waldo_haversine_initial_bearing(double λ1, double φ1,
				double λ2, double φ2) {
    double Δλ = λ2 - λ1;
    double y  = sin(Δλ) * cos(φ2);
    double x  = cos(φ1) * sin(φ2) - sin(φ1) * cos(φ2) * cos(Δλ);
    double θ  = atan2(y, x);

    return θ;
}
        
double
waldo_haversine_final_bearing(double λ1, double φ1,
			      double λ2, double φ2) {
    double θ = M_PI + waldo_haversine_initial_bearing(λ2, φ2, λ1, φ1);
    return θ;
} 
