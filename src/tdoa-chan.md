CHAN idea         : https://programmersought.com/article/50617147592/
Code adapted from : https://programmersought.com/article/336610767583/

~~~
function [POS1,POS2,POS3,POS4] = TDOA_Location(r,bs,Q,c)
N = size(bs,1);
K = bs(:,1).^2 + bs(:,2).^2 + bs(:,3).^2;   %K1 and Ki,i=2,...,M
ha = 0.5*(r.^2-K(2:N)+K(1));   %H matrix
Ga = -[bs(2:N,1)-bs(1,1) bs(2:N,2)-bs(1,2) bs(2:N,3)-bs(1,3) r]; %GA matrix

%%  First WLS results (far field model algorithm, rough model)
Za1 = inv(Ga.'*inv(Q)*Ga)*Ga.'*inv(Q)*ha;
WLS1_far=Za1(1:3);     %The first step WLS far field model results

%%  First WL result (near field model algorithm, precise model)
W1=Za1(1)*ones(N-1,1);
W2=Za1(2)*ones(N-1,1);
W3=Za1(3)*ones(N-1,1);
Ba=diag(sqrt(((W1-bs(2:N,1)).^2)+((W2-bs(2:N,2)).^2)+((W3-bs(2:N,3)).^2)));%B matrix, use ZA1 results to estimate  
Fa = c^2*Ba*Q*Ba;      %F=c^2*B*Q*B matrix

Za2 = inv(Ga.'*inv(Fa)*Ga)*Ga.'*inv(Fa)*ha;
WLS1_near=Za2(1:3);    %First step WLS near field model results


%%  Second WLS results (near field model algorithm, precise model)
W1=Za2(1)*ones(N-1,1);
W2=Za2(2)*ones(N-1,1);
W3=Za2(3)*ones(N-1,1);
Ba=diag(sqrt(((W1-bs(2:N,1)).^2)+((W2-bs(2:N,2)).^2)+((W3-bs(2:N,3)).^2)));%Update the B matrix, estimate with ZA2 results  
Fa = c^2*Ba*Q*Ba;            %Update F matrix

Gb = [1 0 0;0 1 0;0 0 1;1 1 1];          %Ga 'matrix
Bb = diag([Za2(1)-bs(1,1), Za2(2)-bs(1,2),Za2(3)-bs(1,3), norm(Za2(1:3)-bs(1,:)')]); %B'Matrix 
cov_Za=inv(Ga.'*inv(Fa)*Ga); %cov(Za)
Fb = 4*Bb*cov_Za*Bb;         %F'=4B'cov(za)B'
h=[(Za2(1)-bs(1,1))^2;(Za2(2)-bs(1,2))^2;(Za2(3)-bs(1,3))^2;(Za2(4))^2]; %h 'matrix  
Zb1= inv(Gb.'*inv(Fb)*Gb)*Gb.'*inv(Fb)*h;   
WLS2_near=sqrt(Zb1)+bs(1,:)';%The second step WLS near field model results


%%  Second WLS results (far field model algorithm, rough model) 
Bb = diag([Za1(1)-bs(1,1), Za1(2)-bs(1,2), Za1(3)-bs(1,3), norm(Za1(1:3)-bs(1,:)')]); %B'Matrix  
Zb2= inv(Gb.'*inv(Bb)*Ga.'*inv(Q)*Ga*inv(Bb)*Gb)*Gb.'*inv(Bb)*Ga.'*inv(Q)*Ga*inv(Bb)*h;    
WLS2_far=sqrt(Zb2)+bs(1,:)';                %Step 2 WLS far field model results


%%  Output result
POS1=WLS1_near;
POS2=WLS1_far;
POS3=WLS2_near;
POS4=WLS2_far;
~~~


~~~
clear all; 
clc; 

BS1=[-2,0,3];
BS2=[8,-2,-5];
BS3=[12,-5,7];
BS4=[20,1,-13];
BS5=[-3,-12,9];
MS=[300,220,260]; 
A=[BS1;BS2;BS3;BS4;BS5];

std_var=[1e-9]; %TDOA measurement value accuracy range
number=10000; 
c=3e8;

for j=1:length(std_var)  
    num=0;
    [m,~]=size(A);
    Q=ones(m-1)*0.5*std_var(j)^2+diag(ones(m-1,1)*0.5*std_var(j)^2);
    for i=1:number 
          r1=A-ones(5,1)*MS;
          r2=(sum(r1.^2,2)).^(1/2); 
          r=r2(2:end,:)-ones(4,1)*r2(1,:)+std_var(j)*randn(4,1);
          [~,~,result_far21,~]=TDOA_Location(r,A,Q,c); 
      end 
end

figure(1);
plot3(MS(:,1),MS(:,2),MS(:,3),'r*');
hold on;
plot3(result_far21(1,:),result_far21(2,:),result_far21(3,:),'b--o');
hold on;
plot3(A(:,1),A(:,2),A(:,3),'g*');
hold on;
legend('True position','Positioning position','Satellite position');
xlabel('X m');
ylabel('Y m');
zlabel('Z m');
~~~

