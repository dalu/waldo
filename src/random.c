#include <math.h>

#include "waldo.h"
#include "waldo/random.h"

// see: https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
// see: https://karthikkaranth.me/blog/generating-random-points-in-a-sphere/

void
waldo_random_xy_circle(waldo_xy_t *xy, waldo_real_t radius)
{
    if (radius <= 0.0) {
	xy->x = xy->y = 0.0;
	return;
    }
    
    double theta = waldo_random_double() * 2.0 * M_PI;
    xy->x = radius * cos(theta);
    xy->y = radius * sin(theta);
}

void
waldo_random_xy_disc(waldo_xy_t *xy, waldo_real_t radius)
{
    if (radius <= 0.0) {
	xy->x = xy->y = 0.0;
	return;
    }
    
    double r     = sqrt(waldo_random_double());
    double theta = waldo_random_double() * 2.0 * M_PI;
    xy->x = radius * r * cos(theta);
    xy->y = radius * r * sin(theta);
}

void
waldo_random_xy_square(waldo_xy_t *xy, waldo_real_t side)
{
    if (side <= 0.0) {
	xy->x = xy->y = 0.0;
	return;
    }
    
    xy->x = (waldo_random_real() - 0.5) * side;
    xy->y = (waldo_random_real() - 0.5) * side;
}

void
waldo_random_xyz_sphere(waldo_xyz_t *xyz, waldo_real_t radius)
{
    if (radius <= 0.0) {
	xyz->x = xyz->y = xyz->z = 0.0;
	return;
    }

    double theta    = waldo_random_double() * 2.0 * M_PI;
    double phi      = acos(2.0 * waldo_random_double() - 1.0);
    double sinTheta = sin(theta);
    double cosTheta = cos(theta);
    double sinPhi   = sin(phi);
    double cosPhi   = cos(phi);
    xyz->x = radius * sinPhi * cosTheta;
    xyz->y = radius * sinPhi * sinTheta;
    xyz->z = radius * cosPhi;
}

void
waldo_random_xyz_ball(waldo_xyz_t *xyz, waldo_real_t radius)
{
    if (radius <= 0.0) {
	xyz->x = xyz->y = xyz->z = 0.0;
	return;
    }
	
    double theta    = waldo_random_double() * 2.0 * M_PI;
    double phi      = acos(2.0 * waldo_random_double() - 1.0);
    double r        = cbrt(waldo_random_double());
    double sinTheta = sin(theta);
    double cosTheta = cos(theta);
    double sinPhi   = sin(phi);
    double cosPhi   = cos(phi);
    xyz->x = radius * r * sinPhi * cosTheta;
    xyz->y = radius * r * sinPhi * sinTheta;
    xyz->z = radius * r * cosPhi;
}

void
waldo_random_xyz_hollow_ball(waldo_xyz_t *xyz,
			     waldo_real_t r1, waldo_real_t r2)
{
    if ((r1 <= 0.0) || (r2 <= 0.0)) {
	xyz->x = xyz->y = xyz->z = 0.0;
	return;
    }

    double theta    = waldo_random_double() * 2.0 * M_PI;
    double phi      = acos(2.0 * waldo_random_double() - 1.0);
    double rmin     = r1 < r2 ? r1 : r2;
    double rmax     = r1 < r2 ? r2 : r1;
    double r1min    = pow(rmin / rmax, 3);
    double r        = cbrt(r1min + waldo_random_double() * (1 - r1min));
    double sinTheta = sin(theta);
    double cosTheta = cos(theta);
    double sinPhi   = sin(phi);
    double cosPhi   = cos(phi);
    xyz->x = rmax * r * sinPhi * cosTheta;
    xyz->y = rmax * r * sinPhi * sinTheta;
    xyz->z = rmax * r * cosPhi;
}


void
waldo_random_xyz_cube(waldo_xyz_t *xyz, waldo_real_t side)
{
    if (side <= 0.0) {
	xyz->x = xyz->y = xyz->z = 0.0;
	return;
    }
    
    xyz->x = (waldo_random_real() - 0.5) * side;
    xyz->y = (waldo_random_real() - 0.5) * side;
    xyz->z = (waldo_random_real() - 0.5) * side;
}
