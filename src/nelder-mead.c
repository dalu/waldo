// MIT
// http://www.scholarpedia.org/article/Nelder-Mead_algorithm
// https://github.com/matteotiziano/nelder-mead


// cc -I include -I examples src/nelder-mead.c examples/world.c  -lm



#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <waldo.h>
#include <waldo/optimizer.h>

#ifndef WALDO_NELDER_MEAD_RHO
#define WALDO_NELDER_MEAD_RHO		1.0
#endif

#ifndef WALDO_NELDER_MEAD_CHI
#define WALDO_NELDER_MEAD_CHI		2.0
#endif

#ifndef WALDO_NELDER_MEAD_GAMMA
#define WALDO_NELDER_MEAD_GAMMA		0.5
#endif

#ifndef WALDO_NELDER_MEAD_SIGMA
#define WALDO_NELDER_MEAD_SIGMA		0.5
#endif

#ifndef WALDO_NELDER_MEAD_SIMPLEX_STEPSIZE
//#define WALDO_NELDER_MEAD_SIMPLEX_STEPSIZE(x,f)	\
//    ((x) != 0.0 ? 1.05 * (x) : 0.00025)
#define WALDO_NELDER_MEAD_SIMPLEX_STEPSIZE(x,f)	\
    (f)
#endif


static int
_simplex_compare_point(const void *a, const void *b) {
    const waldo_real_t fx1 = ((const waldo_optimizer_point_t *)a)->fx;
    const waldo_real_t fx2 = ((const waldo_optimizer_point_t *)b)->fx;
    return (fx1 > fx2) - (fx1 < fx2);
}

static inline void
_simplex_sort(waldo_optimizer_simplex_t *simplex) {
    qsort((void *)(simplex->point), simplex->n + 1,
	  sizeof(waldo_optimizer_point_t), _simplex_compare_point);
}

static inline void
_simplex_best_side_centroid(waldo_optimizer_simplex_t *simplex,
		 waldo_real_t *centroid) {
    for (int i = 0 ; i < simplex->n ; i++) {
	centroid[i] = 0.0;
	for (int j = 0 ; j < simplex->n ; j++) {
	    centroid[i] += simplex->point[j].x[i];
	}
	centroid[i] /= simplex->n;
    }
}

static inline void
_simplex_delta(waldo_optimizer_simplex_t *simplex,
	       waldo_real_t *delta_x, waldo_real_t *delta_fx) {
    const int n = simplex->n;
    waldo_real_t d_fx = simplex->point[n].fx - simplex->point[0].fx;
    waldo_real_t d_x  = -1.0;
    for (int i = 1; i < n + 1; i++) {
	for (int j = 0; j < n; j++) {
	    const waldo_real_t diff = fabs(simplex->point[0].x[j] -
					   simplex->point[i].x[j]);
	    d_x = WALDO_MAX(d_x, diff);
	}
    }
    *delta_x  = d_x;
    *delta_fx = d_fx;
}


int
waldo_optimizer_nelder_mead(int n,
			    waldo_optimizer_cost_function_t cost_function,
			    const void                     *cost_args,
			    const waldo_real_t             *start,
			    waldo_real_t                   *estimate,
			    const waldo_optimizer_limits_t *limits,
			    waldo_optimizer_stats_t        *stats)
{
    WALDO_ASSERT((limits->epsilon_x  > 0.0) ||
		 (limits->epsilon_fx > 0.0));
    WALDO_ASSERT((limits->max_iter   > 0  ) ||
		 (limits->max_eval   > 0  ));
    
#define _SAVE_STATS							\
    if (stats) {							\
	stats->delta_x  = delta_x;					\
	stats->delta_fx = delta_fx;					\
	stats->iter     = iter_count;					\
	stats->eval     = eval_count;					\
    }

#define _COST_POINT(p)							\
    do {								\
	(p)->fx = cost_function(n, (p)->x, cost_args);			\
	eval_count++;							\
    } while(0)
    
#define _SWAP_POINT(a, b) 						\
    WALDO_SWAP(a, b)

#define _UPDATE_POINT(s /* simplex */, c /* centroid */,		\
		      l /* lambda  */, p /* point    */)		\
    do {								\
	const int n = (s)->n;						\
	for (int i = 0 ; i < n ; i++) {					\
	    (p)->x[i] = (1.0 + (l))*(c)[i] - (l)*(s)->point[n].x[i];	\
	}								\
	_COST_POINT(p);						\
    } while(0)

#define _COPY_POINT(src, dst)						\
    do {								\
	memcpy((dst)->x, (src)->x, sizeof(waldo_real_t) * n);		\
	(dst)->fx = (src)->fx;						\
    } while(0)


    int iter_count = 0;
    int eval_count = 0;
    int rc         = WALDO_OK_OPTIMIZER_CONVERGE;

    waldo_real_t centroid[n];

    // Internal points
    waldo_real_t data_point_r[n];
    waldo_real_t data_point_e[n];
    waldo_real_t data_point_c[n];
    waldo_optimizer_point_t point_r = { .x = data_point_r };
    waldo_optimizer_point_t point_e = { .x = data_point_e };
    waldo_optimizer_point_t point_c = { .x = data_point_c };

    // Initial simplex
    waldo_real_t              simplex_data_point[n * (n+1)];
    waldo_optimizer_point_t   simplex_point[n+1];    
    waldo_optimizer_simplex_t simplex = { .n = n, .point = simplex_point };
    for (int i = 0 ; i < n + 1 ; i++) {
	simplex.point[i].x = &simplex_data_point[i * n];
	
	for (int j = 0 ; j < n ; j++) {
	    const waldo_real_t x_j = start[j];
	    simplex.point[i].x[j] = (i - 1 == j)
		? x_j + WALDO_NELDER_MEAD_SIMPLEX_STEPSIZE(x_j, 1.0)
		: x_j;
	}

	_COST_POINT(&simplex.point[i]);
    }
    
    _simplex_sort(&simplex);
    _simplex_best_side_centroid(&simplex, centroid);
    iter_count++;

    // Iterate until termination
    while(1) {
	/* Check for termination
	 */
	waldo_real_t delta_x, delta_fx;
	_simplex_delta(&simplex, &delta_x, &delta_fx);
	
	// terminate if both delta_x and delta_fx condition are met
	if (((limits->epsilon_x  <= 0.0) || (delta_x  <= limits->epsilon_x )) &&
	    ((limits->epsilon_fx <= 0.0) || (delta_fx <= limits->epsilon_fx))) {
	    _SAVE_STATS;
	    break;
	}

	// terminate if evaluation or iteration count exceeded
	if (((limits->max_eval > 0) && (eval_count > limits->max_eval)) ||
	    ((limits->max_iter > 0) && (iter_count > limits->max_iter))) {
	    _SAVE_STATS;
	    rc = WALDO_OK_OPTIMIZER_DIVERGE;
	    break;
	}


	/* Perform optimization
	 */
	int shrink = 0;
	
	_UPDATE_POINT(&simplex, centroid, WALDO_NELDER_MEAD_RHO, &point_r);

	if (point_r.fx < simplex.point[0].fx) {
	    _UPDATE_POINT(&simplex, centroid,
			  WALDO_NELDER_MEAD_RHO * WALDO_NELDER_MEAD_CHI,
			  &point_e);

	    if (point_e.fx < point_r.fx) {
		// expand
		_COPY_POINT(&point_e, &simplex.point[n]);
	    } else {
		// reflect
		_COPY_POINT(&point_r, &simplex.point[n]);
	    }
	} else {
	    if (point_r.fx < simplex.point[n-1].fx) {
		// reflect
		_COPY_POINT(&point_r, &simplex.point[n]);
	    } else {
		if (point_r.fx < simplex.point[n].fx) {
		    _UPDATE_POINT(&simplex, centroid,
				  WALDO_NELDER_MEAD_RHO*WALDO_NELDER_MEAD_GAMMA,
				  &point_c);
		    
		    if (point_c.fx <= point_r.fx) {
			// contract outside
			_COPY_POINT(&point_c, &simplex.point[n]);
		    } else {
			// shrink
			shrink = 1;
		    }
		} else {
		    _UPDATE_POINT(&simplex, centroid,
				  -WALDO_NELDER_MEAD_GAMMA,
				  &point_c);
		    
		    if (point_c.fx <= simplex.point[n].fx) {
			// contract inside
			_COPY_POINT(&point_c, &simplex.point[n]);
		    } else {
			// shrink
			shrink = 1;
		    }
		}
	    }
	}

	if (shrink) {
	    for (int i = 1; i < n + 1; i++) {
		for (int j = 0; j < n; j++) {
		    simplex.point[i].x[j] = simplex.point[0].x[j] +
			WALDO_NELDER_MEAD_SIGMA * (simplex.point[i].x[j] -
						   simplex.point[0].x[j]);
		}
		_COST_POINT(&simplex.point[i]);
	    }
	    _simplex_sort(&simplex);
	} else {
	    for (int i = n - 1                                         ;
		 i >= 0 && simplex.point[i+1].fx < simplex.point[i].fx ;
		 i--) {
		_SWAP_POINT(simplex.point[i+1], simplex.point[i]);
	    }
	}

	_simplex_best_side_centroid(&simplex, centroid);
	iter_count++;
    }
    
    /* Return result
     */
    memcpy(estimate, simplex.point[0].x, n * sizeof(waldo_real_t));
    return rc;
    
#undef   _COST_POINT
#undef   _SWAP_POINT
#undef _UPDATE_POINT
#undef   _COPY_POINT
}

