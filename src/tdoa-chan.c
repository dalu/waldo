/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * CHAN idea         : https://programmersought.com/article/50617147592/
 * Code adapted from : https://programmersought.com/article/336610767583/
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <waldo.h>
#include <waldo/matrix.h>


static int
waldo_tdoa_chan_covZa(waldo_matrix_t *covZa,
		      waldo_matrix_t *Ga, waldo_matrix_t *Fa) {
    WALDO_ASSERT_MATRIX_IS_SQUARE(Fa);
    WALDO_ASSERT_MATRIX_IS_SIZE(covZa, Ga->columns, Ga->columns);

    int rc = WALDO_ERR_UNKNOWN;
    
    // Transpose Ga
    WALDO_MATRIX_DEF_FROM_TRANSPOSE(Ga_t, Ga);
    waldo_matrix_transpose(&Ga_t, Ga);

    // Inverse Fa
    WALDO_MATRIX_DEF_FROM(Fa_inv, Fa);
    if ((rc = waldo_matrix_inv(&Fa_inv, Fa)) < 0)
	return rc;

    // Gat_Fainv = Ga.' * inv(Fa)
    WALDO_MATRIX_DEF_FROM_MULT(Gat_Fainv, &Ga_t, &Fa_inv);
    waldo_matrix_mult(&Gat_Fainv, &Ga_t, &Fa_inv);

    // covZa = inv(Ga.' * inv(Fa) * Ga)
    WALDO_MATRIX_DEF_FROM_MULT(a, &Gat_Fainv, Ga);
    WALDO_MATRIX_DEF_FROM(a_inv, &a);
    waldo_matrix_mult(&a,  &Gat_Fainv, Ga);
    if ((rc = waldo_matrix_inv(covZa, &a)) < 0)
	return rc;

    return WALDO_OK;
}    

// F = c^2 * B * Q * B
static void
waldo_tdoa_chan_F(waldo_matrix_t *F,
		  waldo_matrix_t *B, waldo_matrix_t *Q, waldo_real_t c) {
    WALDO_ASSERT_MATRIX_IS_SQUARE(Q);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(B, Q);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(F, Q);

    WALDO_MATRIX_DEF_FROM(BQ, Q);

    waldo_matrix_mult(&BQ, B, Q);
    waldo_matrix_mult(F, &BQ, B);
    waldo_matrix_scale(F, F, c * c);
}


static void
waldo_tdoa_chan_Ba(waldo_matrix_t *Ba,
		   waldo_matrix_t *bs, waldo_matrix_t *Za) {
    WALDO_ASSERT_MATRIX_IS_SQUARED_SIZE(Ba, bs->rows-1);

    waldo_matrix_mk_zero(Ba);

    for (int i = 0 ; i < bs->rows-1 ; i++) {
	for (int x = 0 ; x < 3 ; x++) {
	    WALDO_MATRIX_ELT(Ba, i, i) +=
		pow(WALDO_MATRIX_ELT(Za, x, 0) -
		    WALDO_MATRIX_ELT(bs, i+1, x), 2);
	}
	WALDO_MATRIX_ELT(Ba, i, i) = sqrt(WALDO_MATRIX_ELT(Ba, i, i));
    }
}

static void
waldo_tdoa_chan_Bb(waldo_matrix_t *Bb,
	     waldo_matrix_t *bs, waldo_matrix_t *Za) {
    WALDO_ASSERT_MATRIX_IS_SQUARED_SIZE(Bb, Za->rows);

    waldo_matrix_mk_zero(Bb);

    for (int x = 0 ; x < 3 ; x++) {
	waldo_real_t diff = WALDO_MATRIX_ELT(Za, x, 0) -
	                    WALDO_MATRIX_ELT(bs, 0, x);
	WALDO_MATRIX_ELT(Bb, x, x) = diff;
	WALDO_MATRIX_ELT(Bb, 3, 3) += diff * diff;
    }
    WALDO_MATRIX_ELT(Bb, 3, 3) = sqrt(WALDO_MATRIX_ELT(Bb, 3, 3));
}



// Z = inv(G.' *inv(Q) * G) * G.' * inv(Q) * h;
static int
waldo_tdoa_chan_Z(waldo_matrix_t *Z,
		  waldo_matrix_t *G, waldo_matrix_t *Q, waldo_matrix_t *h) {
    WALDO_ASSERT_MATRIX_IS_SQUARE(Q);
    WALDO_ASSERT_MATRIX_IS_SIZE(Z, G->columns, 1);

    int rc = WALDO_ERR_UNKNOWN;
    
    // Transpose G
    WALDO_MATRIX_DEF_FROM_TRANSPOSE(G_t, G);
    waldo_matrix_transpose(&G_t, G);

    // Inverse Q
    WALDO_MATRIX_DEF_FROM(Q_inv, Q);
    if ((rc = waldo_matrix_inv(&Q_inv, Q)) < 0)
	return rc;

    // Gt_Qinv = G.' * inv(Q)
    WALDO_MATRIX_DEF_FROM_MULT(Gt_Qinv, &G_t, &Q_inv);
    waldo_matrix_mult(&Gt_Qinv, &G_t, &Q_inv);

    // Z = inv(Gt_Qinv * G) * Gt_Qinv * h;
    WALDO_MATRIX_DEF_FROM_MULT(a, &Gt_Qinv, G);
    WALDO_MATRIX_DEF_FROM     (a_inv, &a);
    WALDO_MATRIX_DEF_FROM_MULT(b, &a_inv, &Gt_Qinv);
    waldo_matrix_mult(&a, &Gt_Qinv, G);
    if ((rc = waldo_matrix_inv(&a_inv, &a)) < 0)
	return rc;    
    waldo_matrix_mult(&b, &a_inv, &Gt_Qinv);
    waldo_matrix_mult(Z, &b, h);

    return WALDO_OK;
}


/**
 * https://programmersought.com/article/336610767583/
 *
 * @param td pointer to Nx1 matrix
 *           holding time difference of arrival with first base station
 * @param bs pointer to Mx3 matrix
 *           holding the base station coordinates (in 3d)
 * @param q  covariance matrix
 * @param c  propopagation speed
 *
 * @return 0-4 (a_far, a_near, b_near, b_far)
 */
static int
waldo_tdoa_chan3d(waldo_matrix_t *td, waldo_matrix_t *bs,
	    waldo_matrix_t *Q, waldo_real_t c,
	    waldo_xyz_t *a_far,  waldo_xyz_t *a_near, 
	    waldo_xyz_t *b_near, waldo_xyz_t *b_far) {
    WALDO_ASSERT_MATRIX_IS_SIZE(td, bs->rows-1, 1);
    WALDO_ASSERT_MATRIX_IS_SIZE(bs, bs->rows,   3);

    waldo_xyz_t val;
	
    int computed   = 0;
    int bs_count   = bs->rows;
    int td_count   = bs_count - 1;
    int xyzr_count = 4;
    int xyz_count  = 3;
    int max_count  = WALDO_MAX(td_count, xyzr_count);

    // ha and Ga
    WALDO_MATRIX_DEF(ha,   td_count, 1);
    WALDO_MATRIX_DEF(Ga,   td_count, xyzr_count);
    {
	// Diff distance of arrival with first base station (bs1)
	waldo_real_t r[td->rows];
	for (int i = 0 ; i < td->rows ; i++)
	    r[i] = WALDO_MATRIX_ELT(td, i, 0) * c;

	// Base station square distance diff from first base station 
	waldo_real_t K[bs->rows]; 
	for (int i = 0 ; i < bs->rows ; i++)
	    K[i] = WALDO_MATRIX_ELT(bs, i, 0) * WALDO_MATRIX_ELT(bs, i, 0) +
		   WALDO_MATRIX_ELT(bs, i, 1) * WALDO_MATRIX_ELT(bs, i, 1) +
		   WALDO_MATRIX_ELT(bs, i, 2) * WALDO_MATRIX_ELT(bs, i, 2);
	for (int i = 1 ; i < bs->rows ; i++)
	    K[i] -= K[0];

	// ha
	for (int i = 0 ; i < td_count ; i++)
	    WALDO_MATRIX_ELT(&ha, i, 0) = 0.5 * (r[i]*r[i] - K[i+1]);

	// Ga
	for (int i = 0 ; i < td_count ; i++) {
	    for (int x = 0 ; x < 3 ; x++)
		WALDO_MATRIX_ELT(&Ga, i, x) =
		    WALDO_MATRIX_ELT(bs, 0, x) - WALDO_MATRIX_ELT(bs, i+1, x);
	    WALDO_MATRIX_ELT(&Ga, i, 3) = -r[i];
	}
    }

    WALDO_MATRIX_DEF(Za1, xyzr_count, 1);
    if (waldo_tdoa_chan_Z(&Za1, &Ga, Q, &ha) < 0)
	return computed;


    
    // ==> Far field model algorithm (rough model)
    for (int x = 0 ; x < 3 ; x++)
	val.axis[x] = WALDO_MATRIX_ELT(&Za1, x, 0);
    if (waldo_xyz_isnan(&val)) return computed;
    waldo_xyz_copy(&val, a_far);
    computed++;


    
    // Ba
    WALDO_MATRIX_SQUARE_DEF(Ba, td_count);
    waldo_tdoa_chan_Ba(&Ba, bs, &Za1);

    // Fa = c^2*Ba*Q*Ba;
    WALDO_MATRIX_SQUARE_DEF(Fa, td_count);
    waldo_tdoa_chan_F(&Fa, &Ba, Q, c);

    // Za2 = inv(Ga.'*inv(Fa)*Ga)*Ga.'*inv(Fa)*ha;
    WALDO_MATRIX_DEF(Za2, xyzr_count, 1);
    if (waldo_tdoa_chan_Z(&Za2, &Ga, &Fa, &ha) < 0)
	return computed;

    // => Near field model algorithm (precise model)
    for (int x = 0 ; x < 3 ; x++)
	val.axis[x] = WALDO_MATRIX_ELT(&Za2, x, 0);
    if (waldo_xyz_isnan(&val)) return computed;
    waldo_xyz_copy(&val, a_near);
    computed++;

    
    waldo_matrix_t Gb = {
	.size = { 4, 3 },
	.data = (waldo_real_t[]) {
	    1, 0, 0,
	    0, 1, 0,
	    0, 0, 1,
	    1, 1, 1,
	}
    };

    
    WALDO_MATRIX_DEF(Bb, 4, 4);
    waldo_tdoa_chan_Bb(&Bb, bs, &Za2);

    WALDO_MATRIX_DEF(covZa, 4, 4);
    if (waldo_tdoa_chan_covZa(&covZa, &Ga, &Fa) < 0)
	return computed;

    WALDO_MATRIX_DEF(Fb, 4, 4);
    waldo_tdoa_chan_F(&Fb, &Bb, &covZa, 2);



    WALDO_MATRIX_DEF(h, 4, 1);
    for (int x = 0 ; x < 3 ; x++)
	WALDO_MATRIX_ELT(&h, x, 0) = pow(WALDO_MATRIX_ELT(&Za2, x, 0) -
					 WALDO_MATRIX_ELT(bs, 0, x), 2);
    WALDO_MATRIX_ELT(&h, 3, 0) = pow(WALDO_MATRIX_ELT(&Za2, 3, 0), 2);
	

    WALDO_MATRIX_DEF(Zb1, 3, 1);
    if (waldo_tdoa_chan_Z(&Zb1, &Gb, &Fb, &h) < 0)
	return computed;

    // => Near field model algorithm (precise model)
    for (int x = 0 ; x < 3 ; x++)
	val.axis[x] = sqrt(WALDO_MATRIX_ELT(&Zb1, x, 0)) +
	               WALDO_MATRIX_ELT(bs, 0, x);
    if (waldo_xyz_isnan(&val)) return computed;
    waldo_xyz_copy(&val, b_near);
    computed++;

    waldo_tdoa_chan_Bb(&Bb, bs, &Za1);

    WALDO_MATRIX_DEF_FROM(Bb_inv, &Bb);
    waldo_matrix_inv(&Bb_inv, &Bb);
    
    WALDO_MATRIX_DEF_FROM(Ga_Bbinv, &Ga);
    waldo_matrix_mult(&Ga_Bbinv, &Ga, &Bb_inv);

    WALDO_MATRIX_DEF(hb2, td_count, 1);
    waldo_matrix_mult(&hb2, &Ga_Bbinv, &h);

    WALDO_MATRIX_DEF(Ga_Bbinv_Gb, td_count, 3);
    waldo_matrix_mult(&Ga_Bbinv_Gb, &Ga_Bbinv, &Gb);

    WALDO_MATRIX_DEF(Zb2, 3, 1);
    if (waldo_tdoa_chan_Z(&Zb2, &Ga_Bbinv_Gb, Q, &hb2) < 0)
	return computed;

    // ==> Far field model algorithm (rough model)
    for (int x = 0 ; x < 3 ; x++)
	val.axis[x] = sqrt(WALDO_MATRIX_ELT(&Zb2, x, 0)) +
	               WALDO_MATRIX_ELT(bs, 0, x);
    if (waldo_xyz_isnan(&val)) return computed;
    waldo_xyz_copy(&val, b_far);
    computed++;

    // Fully done
    return computed;
}





int waldo_xyz_tdoa_chan_ab(int size, waldo_xyzt_t *bases, waldo_real_t velocity,
			   waldo_xyz_t *a_far,  waldo_xyz_t *a_near,
			   waldo_xyz_t *b_near, waldo_xyz_t *b_far) {

    // Build base station matrix
    WALDO_MATRIX_DEF(bs, size, 3);
    for (int i = 0 ; i < size ; i++)
	for (int x = 0 ; x < 3 ; x++)
	    WALDO_MATRIX_ELT(&bs, i, x) = bases[i].axis[x];

    // Build time difference matrix
    WALDO_MATRIX_DEF(td, size-1, 1);
    for (int i = 1 ; i < size ; i++)
	WALDO_MATRIX_ELT(&td, i-1, 0) = bases[i].t - bases[0].t;

    // Use default covariant matrix Q (1 on diagonal, 1/2 otherwise)
    WALDO_MATRIX_SQUARE_DEF(Q, size-1);
    for (int i = 0 ; i < Q.rows ; i++)
	for (int j = 0 ; j < Q.columns ; j++)
	    WALDO_MATRIX_ELT(&Q, i, j) = (i == j) ? 1.0 : 0.5;

    // TDOA
    return waldo_tdoa_chan3d(&td, &bs, &Q, velocity,
			     a_far, a_near, b_near, b_far);
}


int waldo_xyz_tdoa_chan(int size, waldo_xyzt_t *bases, waldo_real_t velocity,
			waldo_xyz_t *near, waldo_xyz_t *far) {
    waldo_xyz_t a_far  = WALDO_XYZ_NAN;
    waldo_xyz_t a_near = WALDO_XYZ_NAN;
    waldo_xyz_t b_near = WALDO_XYZ_NAN;
    waldo_xyz_t b_far  = WALDO_XYZ_NAN;

    int n = waldo_xyz_tdoa_chan_ab(size, bases, velocity,
				   &a_far, &a_near, &b_near, &b_far);
    if (n < 0 ) return n;
    if (n <= 2)	goto single;
    
    double e_a = 0, e_b = 0;
    for (int i = 1 ; i < size ; i++) {
	double td = bases[i].t - bases[0].t;
	e_a += pow(((waldo_xyz_dist(&bases[i].xyz, &a_near) - 
		     waldo_xyz_dist(&bases[0].xyz, &a_near)) / velocity) -
		   td, 2);
	e_b += pow(((waldo_xyz_dist(&bases[i].xyz, &b_near) - 
		     waldo_xyz_dist(&bases[0].xyz, &b_near)) / velocity) -
		   td, 2);
    }

    if (e_a < e_b) {
    single:
	waldo_xyz_copy(&a_near, near);
	waldo_xyz_copy(&a_far,  far );
    } else {
	waldo_xyz_copy(&b_near, near);
	waldo_xyz_copy(&b_far,  far );
    }

    return WALDO_OK; // XXX: check for failed inversion
}

