#include <stdio.h>

#include <waldo.h>
#include <waldo/matrix.h>


/* https://nhigham.com/2022/05/18/the-big-six-matrix-factorizations/
 * https://en.wikipedia.org/wiki/LU_decomposition
 * https://en.wikipedia.org/wiki/Row_echelon_form
 */


void
waldo_matrix_dump_fmt(waldo_matrix_t *m, char *prefix, char *fmt)
{
    for (size_t i = 0 ; i < m->rows ; i++) {
	if (prefix != NULL)
	    printf("%s ", prefix);
	for (size_t j = 0 ; j < m->columns ; j++) {
	    printf(fmt, WALDO_MATRIX_ELT(m, i, j));
	    printf(" ");
	}
	printf("\n");
    }   
}


void
waldo_matrix_solve_lup(waldo_matrix_t *a, int *p, waldo_real_t *b,
		       waldo_real_t *x) {
    const int n = a->rows;

    for (int i = 0; i < n; i++) {
        x[i] = b[p[i]];

        for (int k = 0; k < i; k++)
            x[i] -= WALDO_MATRIX_ELT(a, i, k) * x[k];
    }

    for (int i = n - 1; i >= 0; i--) {
        for (int k = i + 1; k < n ; k++)
            x[i] -= WALDO_MATRIX_ELT(a, i, k) * x[k];

        x[i] /= WALDO_MATRIX_ELT(a, i, i);
    }
}

waldo_real_t
waldo_matrix_det_lup(waldo_matrix_t *lu, int perms) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(lu);
    WALDO_ASSERT_MATRIX_IS_SQUARE(lu);

    const int n = lu->rows;
    waldo_real_t det = WALDO_MATRIX_ELT(lu, 0, 0);

    for (int i = 1 ; i < n ; i++)
        det *= WALDO_MATRIX_ELT(lu, i, i);

    return perms % 2 == 0 ? det : -det;
}


void
waldo_matrix_inv_lup(waldo_matrix_t *r, waldo_matrix_t *lu, int *p) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(lu);
    WALDO_ASSERT_MATRIX_IS_SQUARE(lu);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(r, lu);
    WALDO_ASSERT(r != lu);
    
    const int n = lu->rows;
    
    for (int j = 0 ; j < n ; j++) {
        for (int i = 0 ; i < n ; i++) {
            WALDO_MATRIX_ELT(r, i, j) = (p[i] == j) ? 1.0 : 0.0;

            for (int k = 0 ; k < i ; k++)
                WALDO_MATRIX_ELT(r, i, j) -= WALDO_MATRIX_ELT(lu, i, k) *
		                             WALDO_MATRIX_ELT(r,  k, j);
        }

        for (int i = n - 1; i >= 0 ; i--) {
            for (int k = i + 1 ; k < n ; k++)
                WALDO_MATRIX_ELT(r, i, j) -= WALDO_MATRIX_ELT(lu, i, k) *
		                             WALDO_MATRIX_ELT(r,  k, j);

	    WALDO_MATRIX_ELT(r, i, j) /= WALDO_MATRIX_ELT(lu, i, i);
        }
    }
}

int
waldo_matrix_lup(waldo_matrix_t *r, waldo_matrix_t *a, int *p)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(r);
    WALDO_ASSERT_MATRIX_IS_DEFINED(a);
    WALDO_ASSERT_MATRIX_IS_SQUARE(a);
    WALDO_ASSERT_MATRIX_IS_SAME_SIZE(r, a);
    
    // Decomposition is done in-place
    //  => perform a copy if necessary
    if (r != a) {
	waldo_matrix_copy(r, a);
	a = r;
    }
    
    const int n = a->rows;
    int count = 0;


    if (p) {
	for (int i = 0 ; i < n ; i++)
	    p[i] = i;
    }

    for (int i = 0 ; i < n ; i++) {
        waldo_real_t a_max = 0.0;
        int          i_max = i;

        for (int k = i ; k < n ; k++) {
	    waldo_real_t a_abs = fabs(WALDO_MATRIX_ELT(a, k, i));
            if (a_abs > a_max) { 
                a_max = a_abs;
                i_max = k;
            }
	}

        if (a_max < WALDO_MATRIX_EPSILON)
	    return WALDO_ERR_MATH;

        if (i_max != i) {
	    if (p) {
		WALDO_SWAP(p[i], p[i_max]);
	    }
	    waldo_matrix_swap_row(a, i, i_max);
            count++;
        }

        for (int j = i + 1; j < n; j++) {
            WALDO_MATRIX_ELT(a, j, i) /= WALDO_MATRIX_ELT(a, i, i);
	    
            for (int k = i + 1; k < n; k++)
                WALDO_MATRIX_ELT(a, j, k) -= WALDO_MATRIX_ELT(a, j, i) *
		                             WALDO_MATRIX_ELT(a, i, k);
        }
    }

    return count;
}


#if 0


void crout(double const **A, double **L, double **U, int n) {
	int i, j, k;
	double sum = 0;

	for (i = 0; i < n; i++) {
		U[i][i] = 1;
	}

	for (j = 0; j < n; j++) {
		for (i = j; i < n; i++) {
			sum = 0;
			for (k = 0; k < j; k++) {
				sum = sum + L[i][k] * U[k][j];	
			}
			L[i][j] = A[i][j] - sum;
		}

		for (i = j; i < n; i++) {
			sum = 0;
			for(k = 0; k < j; k++) {
				sum = sum + L[j][k] * U[k][i];
			}
			if (L[j][j] == 0) {
				printf("det(L) close to 0!\n Can't divide by 0...\n");
				exit(EXIT_FAILURE);
			}
			U[j][i] = (A[j][i] - sum) / L[j][j];
		}
	}
}

#endif

waldo_real_t
waldo_matrix_det_3x3(waldo_matrix_t *m)
{
    WALDO_ASSERT_MATRIX_IS_SQUARE(m);
    WALDO_ASSERT(m->rows == 3);
	/*
	 * 3x3 matrix element to array table:
	 *
	 * 1,1 = 0  1,2 = 1  1,3 = 2
	 * 2,1 = 3  2,2 = 4  2,3 = 5
	 * 3,1 = 6  3,2 = 7  3,3 = 8
	 */

    waldo_real_t d = 0;
    d += WALDO_MATRIX_ELT(m, 0, 0) *
	(WALDO_MATRIX_ELT(m, 1, 1) * WALDO_MATRIX_ELT(m, 2, 2) -
	 WALDO_MATRIX_ELT(m, 2, 1) * WALDO_MATRIX_ELT(m, 1, 2));
    d -= WALDO_MATRIX_ELT(m, 1, 0) *
	(WALDO_MATRIX_ELT(m, 0, 1) * WALDO_MATRIX_ELT(m, 2, 2) -
	 WALDO_MATRIX_ELT(m, 2, 1) * WALDO_MATRIX_ELT(m, 0, 2));
    d += WALDO_MATRIX_ELT(m, 2, 0) *
	(WALDO_MATRIX_ELT(m, 0, 1) * WALDO_MATRIX_ELT(m, 1, 2) -
	 WALDO_MATRIX_ELT(m, 1, 1) * WALDO_MATRIX_ELT(m, 0, 2));
    
    return d;
}




waldo_real_t
waldo_matrix_rref(waldo_matrix_t *rref, waldo_matrix_t *m, waldo_matrix_t *a) {
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT((a == NULL) || ((a != m) && (a != rref)));


    if (rref != m) {
	waldo_matrix_copy(rref, m);
	m = rref;
    }
    
    int          lead = 0;
    waldo_real_t det  = WALDO_MATRIX_IS_SQUARE(m) ? 1.0 : 0.0;
    
    for (int r = 0 ; (r < m->rows) && (lead < m->columns) ; r++) {
	int i = r;

	while (WALDO_MATRIX_ELT(m, i, lead) == 0.0) {
	    i++;
	    if (i == m->rows) {
		lead++;
		if (lead >= m->columns)
		    return det;
		i = r;
	    }
	}

	if (i != r) {
	    det = 0.0;
	    waldo_matrix_swap_row(m, i, r);
	    if (a) waldo_matrix_swap_row(a, i, r);
	}

	waldo_real_t s = WALDO_MATRIX_ELT(m, r, lead);
	det *= s;
	waldo_matrix_div_row(m, r, s);
	if (a) waldo_matrix_div_row(a, r, s);

	for (int k = 0 ; k < m->rows ; k++) {
	    if (k == r) continue;
	    waldo_real_t s = WALDO_MATRIX_ELT(m, k, lead);
	    waldo_matrix_axpy_row(m,k, -s, m,r, m,k);
	    if (a) waldo_matrix_axpy_row(a,k, -s, a,r, a,k);
	}

	lead++;
    }

    return det;
}






#if 0

waldo_real_t 
waldo_matrix_det(waldo_matrix_t *m)
{
    WALDO_ASSERT_MATRIX_IS_DEFINED(m);
    WALDO_ASSERT_MATRIX_IS_SQUARE(m);

    switch(m->rows) {
    case 1: 
	return WALDO_MATRIX_ELT(m, 0, 0);

    case 2:
	return WALDO_MATRIX_ELT(m, 0, 0) * WALDO_MATRIX_ELT(m, 1, 1) -
	       WALDO_MATRIX_ELT(m, 0, 1) * WALDO_MATRIX_ELT(m, 1, 0);

    case 3:
	return WALDO_MATRIX_ELT(m, 0, 0) *
	         (WALDO_MATRIX_ELT(m, 1, 1) * WALDO_MATRIX_ELT(m, 2, 2) -
		  WALDO_MATRIX_ELT(m, 2, 1) * WALDO_MATRIX_ELT(m, 1, 2))
	     - WALDO_MATRIX_ELT(m, 1, 0) *
	         (WALDO_MATRIX_ELT(m, 0, 1) * WALDO_MATRIX_ELT(m, 2, 2) -
		  WALDO_MATRIX_ELT(m, 2, 1) * WALDO_MATRIX_ELT(m, 0, 2))
	     + WALDO_MATRIX_ELT(m, 2, 0) *
	         (WALDO_MATRIX_ELT(m, 0, 1) * WALDO_MATRIX_ELT(m, 1, 2) -
		  WALDO_MATRIX_ELT(m, 1, 1) * WALDO_MATRIX_ELT(m, 0, 2));
    default:

	/* Full calculation required for non 3x3 matrices. */
	int rc;
	zsl_real_t dtmp;
	zsl_real_t cur;
	zsl_real_t sign;
	ZSL_MATRIX_DEF(mr, (m->sz_rows - 1), (m->sz_rows - 1));

	/* Clear determinant output before starting. */
	*d = 0.0;

	/*
	 * Iterate across row 0, removing columns one by one.
	 * Note that these calls are recursive until we reach a 3x3 matrix,
	 * which will be calculated using the shortcut at the top of this
	 * function.
	 */
	for (size_t g = 0; g < m->sz_cols; g++) {
		zsl_mtx_get(m, 0, g, &cur);     /* Get value at (0, g). */
		zsl_mtx_init(&mr, NULL);        /* Clear mr. */
		zsl_mtx_reduce(m, &mr, 0, g);   /* Remove row 0, column g. */
		rc = zsl_mtx_deter(&mr, &dtmp); /* Calc. determinant of mr. */
		sign = 1.0;
		if (rc) {
			return -EINVAL;
		}

		/* Uneven elements are negative. */
		if (g % 2 != 0) {
			sign = -1.0;
		}

		/* Add current determinant to final output value. */
		*d += dtmp * cur * sign;
	}

	return 0;
}



#endif
