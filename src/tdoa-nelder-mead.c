#include <stdlib.h>
#include <waldo.h>
#include <waldo/tdoa.h>
#include <waldo/optimizer.h>



struct _waldo_xyz_tdoa_baseline {
    waldo_xyz_t  a;     // Base A
    waldo_xyz_t  b;     // Base B
    waldo_real_t delta; // Difference
};

struct _waldo_xyz_tdoa {
    int count;
    struct _waldo_xyz_tdoa_baseline *baseline;
};

struct _waldo_xyZ_tdoa {
    int count;
    struct _waldo_xyz_tdoa_baseline *baseline;
    waldo_real_t z;
};



static waldo_real_t
_waldo_xyz_cost_tdoa(int n, waldo_real_t *e, const void *args)
{
    WALDO_ASSERT(n == 3);
    
    waldo_real_t                  r    = 0.0;
    const struct _waldo_xyz_tdoa *tdoa = args;
    waldo_xyz_t                  *pos  = (waldo_xyz_t *)e;

    for (int i = 0 ; i < tdoa->count ; i++) {	
	struct _waldo_xyz_tdoa_baseline *baseline = &tdoa->baseline[i];
	waldo_real_t delta = waldo_xyz_dist(&baseline->a, pos) -
	                     waldo_xyz_dist(&baseline->b, pos);
	r += pow(delta - baseline->delta, 2);
    }

    return r;
}


static waldo_real_t
_waldo_xyZ_cost_tdoa(int n, waldo_real_t *e, const void *args)
{
    WALDO_ASSERT(n == 2);
    
    waldo_real_t                  r       = 0.0;
    const struct _waldo_xyZ_tdoa *tdoa    = args;
    waldo_xy_t                   *pos_xy  = (waldo_xy_t *)e;
    waldo_xyz_t                   pos_xyz = { .x = pos_xy->x, .y = pos_xy->y,
					      .z = tdoa->z };
    
    for (int i = 0 ; i < tdoa->count ; i++) {	
	struct _waldo_xyz_tdoa_baseline *baseline = &tdoa->baseline[i];
	waldo_real_t delta = waldo_xyz_dist(&baseline->a, &pos_xyz) -
	                     waldo_xyz_dist(&baseline->b, &pos_xyz);
	r += pow(delta - baseline->delta, 2);
    }

    return r;
}



int waldo_xyz_tdoa_nlm(int size, waldo_xyzt_t *bases, waldo_real_t velocity,
		       waldo_xyz_t *start, waldo_xyz_t *pos,
		       int iterations, double epsilon) {
    struct _waldo_xyz_tdoa_baseline baseline[size - 1];
    for (int i = 0 ; i < size - 1 ; i++) {
	baseline[i] = (struct _waldo_xyz_tdoa_baseline) {
	    .a.x   = bases[0  ].x,
	    .a.y   = bases[0  ].y,
	    .a.z   = bases[0  ].z,
	    .b.x   = bases[i+1].x,
	    .b.y   = bases[i+1].y,
	    .b.z   = bases[i+1].z,
	    .delta = (bases[0].t - bases[i+1].t) * velocity
	};
    }
    struct _waldo_xyz_tdoa tdoa = {
	.count    = size - 1,
	.baseline = baseline
    };

    waldo_optimizer_limits_t limits = {
	.epsilon_x   = epsilon,
	.epsilon_fx  = epsilon,
	.max_iter    = iterations,
    };
    
    return waldo_optimizer_nelder_mead(3, _waldo_xyz_cost_tdoa, &tdoa,
				       start->axis, pos->axis, &limits, NULL);
}


int waldo_xyZ_tdoa_nlm(int size, waldo_xyzt_t *bases, waldo_real_t velocity,
		       waldo_xyz_t *start, waldo_xyz_t *pos,
		       int iterations, double epsilon) {
    struct _waldo_xyz_tdoa_baseline baseline[size - 1];
    for (int i = 0 ; i < size - 1 ; i++) {
	baseline[i] = (struct _waldo_xyz_tdoa_baseline) {
	    .a.x   = bases[0  ].x,
	    .a.y   = bases[0  ].y,
	    .a.z   = bases[0  ].z,
	    .b.x   = bases[i+1].x,
	    .b.y   = bases[i+1].y,
	    .b.z   = bases[i+1].z,
	    .delta = (bases[0].t - bases[i+1].t) * velocity
	};
    }
    struct _waldo_xyZ_tdoa tdoa = {
	.count    = size - 1,
	.baseline = baseline,
	.z        = start->z,
    };

    waldo_optimizer_limits_t limits = {
	.epsilon_x   = epsilon,
	.epsilon_fx  = epsilon,
	.max_iter    = iterations,
    };
    
    int rc = waldo_optimizer_nelder_mead(2, _waldo_xyZ_cost_tdoa, &tdoa,
				       start->axis, pos->axis, &limits, NULL);
    if (rc < 0) return rc;
    
    pos->z = start->z;
    return rc;
}

